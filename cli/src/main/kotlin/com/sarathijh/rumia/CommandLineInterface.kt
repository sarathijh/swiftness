package com.sarathijh.rumia

import com.sarathijh.rumia.CompilationStage.*
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import java.io.FileOutputStream

fun main(args: Array<String>) = mainBody {
    ArgParser(args).parseInto(::Arguments).run {
        when (compilationStage) {
            TOKENS -> Compiler().lexerTokens(sourceFile, System.out)
            ABSTRACT_SYNTAX_TREE -> Compiler().abstractSyntaxTree(sourceFile, System.out)
            ASSEMBLY -> Compiler().assembly(sourceFile, System.out)
            BINARY -> {
                val outputStream = FileOutputStream(sourceFile.replace(".rumia", ""))
                Compiler().binary(sourceFile, outputStream)
            }
            NES_ROM -> {
                val outputStream = FileOutputStream(sourceFile.replace(".rumia", ".nes"))
                Compiler().nesRom(sourceFile, outputStream)
            }
        }
    }
}
