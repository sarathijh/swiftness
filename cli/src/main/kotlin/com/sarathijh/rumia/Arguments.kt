package com.sarathijh.rumia

import com.sarathijh.rumia.CompilationStage.*
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default

class Arguments(parser: ArgParser) {
    val compilationStage by parser
            .mapping(
                    "--tokens" to TOKENS,
                    "--ast" to ABSTRACT_SYNTAX_TREE,
                    "--assembly" to ASSEMBLY,
                    "--binary" to BINARY,
                    "--nes-rom" to NES_ROM,
                    help = "Compilation stage to perform, default is '--binary'")
            .default(NES_ROM)

    val sourceFile by parser.positional(help = "Source file to compile")
}

enum class CompilationStage {
    TOKENS,
    ABSTRACT_SYNTAX_TREE,
    ASSEMBLY,
    BINARY,
    NES_ROM,
}
