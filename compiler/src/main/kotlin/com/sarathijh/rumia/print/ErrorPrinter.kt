package com.sarathijh.rumia.print

import com.sarathijh.rumia.frontend.lex.SourceLocation
import com.sarathijh.rumia.frontend.lex.SourceRange
import java.io.BufferedReader
import java.io.FileReader
import java.io.PrintStream
import java.lang.Integer.max

class ErrorPrinter(private val output: PrintStream, private val numContextLines: Int = 0, private val markerChar: Char = '^', private val underlineChar: Char = '~') {

    fun printErrorWithContext(filePath: String, message: String, sourceRange: SourceRange) {
        output.println("${sourceRange.string} ${"error:".boldAndRed} $message\n")

        if (sourceRange.endInclusive.isAfter) {
            printMarkAfterSourceLocation(filePath, sourceRange.endInclusive)
        } else {
            sourceRange.printHighlighted(filePath)
        }
    }

    private fun printMarkAfterSourceLocation(filePath: String, sourceLocation: SourceLocation) {
        printLineFromFileWithContext(filePath, sourceLocation.line, numContextLines) { line, lineNumber ->
            val beforeTarget = line.substring(0, sourceLocation.column + 1) + " "
            val afterTarget = line.substring(sourceLocation.column + 1)
            val paddingToTarget = " ".repeat(sourceLocation.column + 1 + lineNumber.length)

            output.println("  $lineNumber $beforeTarget$afterTarget".bold)
            output.println("   $paddingToTarget${markerChar.toString().boldAndRed}")
        }
    }

    private fun SourceRange.printHighlighted(filePath: String) {
        printLineFromFileWithContext(filePath, start.line, numContextLines) { line, lineNumber ->
            val beforeTarget = line.substring(0, start.column)
            val paddingToTarget = " ".repeat(start.column + lineNumber.length)
            val highlightWidth = endInclusive.column - start.column + 1
            val char = if (highlightWidth == 1) markerChar else underlineChar
            val underline = char.toString().repeat(endInclusive.column - start.column + 1).boldAndRed

            output.print("  ${lineNumber.bold} ${beforeTarget.bold}")

            if (start.column + 1 < line.length) {
                val target = line.substring(start.column, endInclusive.column + 1).boldAndRed
                val afterTarget = line.substring(endInclusive.column + 1)

                output.print("$target${afterTarget.bold}")
            }

            output.println()
            output.println("   $paddingToTarget$underline")
        }
    }

    private fun printLineFromFileWithContext(filePath: String, targetLineIndex: Int, numContextLines: Int, lineHandler: (line: String, lineNumber: String) -> Unit) {
        val firstLineIndex = targetLineIndex - numContextLines
        val lastLineIndex = targetLineIndex + numContextLines

        BufferedReader(FileReader(filePath)).readLineRange(firstLineIndex..lastLineIndex) { line, lineNumber ->
            val lineNumberString = lineNumber.toString().padStart(1) + "."

            if (lineNumber == targetLineIndex + 1) {
                lineHandler(line, lineNumberString)
            } else {
                output.println("  $lineNumberString $line")
            }
        }
    }

    companion object {
        private fun BufferedReader.readLineRange(indexRange: IntRange, action: (line: String, lineNumber: Int) -> Unit) {
            if (indexRange.start > 0) {
                skipLines(indexRange.start)
            }

            val firstLine = max(indexRange.start + 1, 1)
            readLines(indexRange.count()) { line, index -> action(line, firstLine + index) }
        }

        private fun BufferedReader.skipLines(n: Int) = (0 until n).forEach { readLine() }

        private fun BufferedReader.readLines(n: Int, action: (String, Int) -> Unit) = (0 until n).forEach {
            try {
                action(readLine(), it)
            } catch (exception: Exception) {
                return@readLines
            }
        }

        private val String.boldAndRed get() = "\u001B[1;31m$this\u001B[0m"
        private val String.bold get() = "\u001B[1;1m$this\u001B[0m"

        private val SourceRange.string get() = "${start.line+1}:${(if (endInclusive.isAfter) endInclusive.column else start.column) + 1}"
    }
}
