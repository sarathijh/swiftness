package com.sarathijh.rumia.print

import com.sarathijh.rumia.frontend.ast.*
import com.sarathijh.rumia.util.forEachBetween
import java.io.PrintStream

class TreePrinter(private val output: PrintStream) {

    var indentLevel = 0

    fun print(node: AstNode) {
        when (node) {
            is BlockStatement -> node.statements.forEachBetween({ print("\n") }, { print(it) })

            is AstNodeList<*> -> {}

            is ExpressionStatement -> {
                printIndent()
                print(node.expression)
            }

            is AssignStatement -> {
                printIndent()
                print("(assign ")
                print(node.identifierExpression)
                print(" ")
                print(node.valueExpression)
                print(")")
            }

            is SubscriptAssignStatement -> {
                printIndent()
                print("(assign ")
                print(node.subscriptExpression)
                print(" ")
                print(node.valueExpression)
                print(")")
            }

            is IfStatement -> {
                printIndent()

                print("(if ")
                print(node.conditionExpression)
                print("\n")

                indented {
                    print(node.bodyStatement)

                    if (node.elseStatement != null) {
                        print("\n")
                        print(node.elseStatement)
                    }
                }

                print(")")
            }

            is WhileStatement -> {
                printIndent()

                print("(while ")
                print(node.conditionExpression)
                print("\n")

                indented {
                    print(node.bodyStatement)
                }

                print(")")
            }

            is ForInStatement -> {
                printIndent()

                print("(for ")
                print(node.identifierExpression)
                print(" in ")
                print(node.arrayExpression)
                print("\n")

                indented {
                    print(node.bodyStatement)
                }

                print(")")
            }

            is ReturnStatement -> {
                printIndent()

                print("(return")

                if (node.valueExpression != null) {
                    print(" ")
                    print(node.valueExpression)
                }

                print(")")
            }


            is VariableDeclaration -> {
                printIndent()
                print("(var ${node.identifierExpression.name}")

                if (node.typeAnnotation != null) {
                    print(" ")
                    print(node.typeAnnotation)
                }

                if (node.valueExpression != null) {
                    print(" ")
                    print(node.valueExpression)
                }

                print(")")
            }

            is FunctionDeclaration -> {
                printIndent()
                print("(fun (${node.identifierExpression.name} -> ")

                if (node.returnTypeAnnotation != null) {
                    print(node.returnTypeAnnotation)
                } else {
                    print("Void")
                }

                print(") (")

                node.parameters.forEachBetween({ print(" ") }, { print(it) })

                print(")\n")

                indented {
                    print(node.bodyStatement)
                }

                print(")")
            }

            is FunctionParameter -> {
                print("(${node.identifierExpression.name} ")
                print(node.typeAnnotation)
                print(")")
            }

            is IdentifierExpression -> print(node.name)
            is IntLiteralExpression -> print(node.intValue)
            is BoolLiteralExpression -> print(if (node.boolValue) "true" else "false")
            is ArrayLiteralExpression -> {
                print("[")
                node.elementExpressions.forEachBetween({ print(" ") }, { print(it) })
                print("]")
            }

            is FunctionCallExpression -> {
                print("(call ")
                print(node.identifierExpression)
                print (" ")

                node.arguments.forEachBetween({ print(" ") }, { print(it) })

                print(")")
            }

            is MemberAccessExpression -> {
                print("(")
                print(node.memberExpression)
                print(" ")
                print(node.objectExpression)
                print(")")
            }

            is SubscriptExpression -> {
                print("(subscript ")
                print(node.identifierExpression)
                print(" ")
                print(node.indexExpression)
                print(")")
            }

            is InfixExpression -> {
                print("(${node.operator} ")
                print(node.leftExpression)
                print(" ")
                print(node.rightExpression)
                print(")")
            }

            is PrefixExpression -> {
                print("(prefix${node.operator} ")
                print(node.subExpression)
                print(")")
            }

            is IntTypeAnnotation -> print("Int")
            is Int16TypeAnnotation -> print("Int16")
            is UIntTypeAnnotation -> print("UInt")
            is UInt16TypeAnnotation -> print("UInt16")
            is BoolTypeAnnotation -> print("Bool")
            is ArrayTypeAnnotation -> {
                print(node.elementTypeAnnotation)
                print("[")
                print(node.sizeExpression)
                print("]")
            }
        }
    }

    private fun print(str: String) = output.print(str)

    private fun printIndent() {
        for (i in 1..indentLevel) {
            output.print("    ")
        }
    }

    private fun indented(block: () -> Unit) {
        indentLevel += 1
        block()
        indentLevel -= 1
    }
}
