package com.sarathijh.rumia

import com.sarathijh.rumia.backend.mos6502.asm.Assembler
import com.sarathijh.rumia.backend.mos6502.asm.printAsm
import com.sarathijh.rumia.backend.mos6502.codegen.CodeGenerator
import com.sarathijh.rumia.backend.nes.NesRomWriter
import com.sarathijh.rumia.frontend.lex.LexError
import com.sarathijh.rumia.frontend.lex.Lexer
import com.sarathijh.rumia.frontend.lex.SourceRange
import com.sarathijh.rumia.frontend.parse.ParseError
import com.sarathijh.rumia.frontend.parse.Parser
import com.sarathijh.rumia.frontend.typecheck.*
import com.sarathijh.rumia.print.ErrorPrinter
import com.sarathijh.rumia.print.TreePrinter
import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.io.PrintStream

class Compiler {
    fun lexerTokens(filePath: String, output: PrintStream) {
        performTask(filePath) {
            Lexer(filePath).forEach {
                val sourceLocationString = "${it.sourceRange.start.line}:" +
                        if (it.sourceRange.start.column == it.sourceRange.endInclusive.column) {
                            "${it.sourceRange.start.column}"
                        } else {
                            "${it.sourceRange.start.column}-${it.sourceRange.endInclusive.column}"
                        }

                if (it.kind == it.value || it.kind == Lexer.TkNewline) {
                    output.println("'${it.kind}' ($sourceLocationString)")
                } else {
                    output.println("'${it.value}' [${it.kind}] ($sourceLocationString)")
                }
            }
        }
    }

    fun abstractSyntaxTree(filePath: String, output: PrintStream) {
        performTask(filePath) {
            val ast = Parser(Lexer(filePath)).parse()

            TreePrinter(output).print(ast)
            output.println()
        }
    }

    fun assembly(filePath: String, output: PrintStream) {
        performTask(filePath) {
            val ast = Parser(Lexer(filePath)).parse()

            TypeChecker(standardTypeEnvironment).typeCheck(ast)

            val asm = CodeGenerator().generate(ast)

            output.printAsm(asm)
        }
    }

    fun binary(filePath: String, output: OutputStream) {
        performTask(filePath) {
            val ast = Parser(Lexer(filePath)).parse()

            TypeChecker(standardTypeEnvironment).typeCheck(ast)

            val asm = CodeGenerator().generate(ast)

            Assembler(output).assemble(asm)
        }
    }

    fun nesRom(filePath: String, output: OutputStream) {
        performTask(filePath) {
            val ast = Parser(Lexer(filePath)).parse()

            TypeChecker(standardTypeEnvironment).typeCheck(ast)

            val asm = CodeGenerator().generate(ast)

            val programCodeOutput = ByteArrayOutputStream()
            Assembler(programCodeOutput).assemble(asm)

            NesRomWriter(output).write(programCodeOutput.toByteArray())
        }
    }

    private fun performTask(filePath: String, task: () -> Unit) {
        try {
            task()

        } catch (lexError: LexError) {
            printError(filePath, lexError.message, lexError.sourceRange)

        } catch (parseError: ParseError) {
            printError(filePath, parseError.message, parseError.sourceRange)

        } catch (typeError: TypeError) {
            printError(filePath, typeError.message, typeError.sourceRange)
        }
    }

    private fun printError(filePath: String, message: String, sourceRange: SourceRange) {
        ErrorPrinter(System.out, 3).printErrorWithContext(filePath, message, sourceRange)
    }

    private val standardTypeEnvironment by lazy {
        val typeEnvironment = TypeEnvironment()

        val ppuClass = ClassType("PPU")
        ppuClass.instanceMemberTypes["controller"] = IntType
        ppuClass.instanceMemberTypes["mask"] = IntType
        typeEnvironment["PPU"] = ppuClass

        typeEnvironment["infix*"] = FunctionType(IntType, listOf(IntType, IntType), true)
        typeEnvironment["infix%"] = FunctionType(IntType, listOf(IntType, IntType), true)

        for (operator in listOf("+", "-", "&", "|", "^")) {
            typeEnvironment["infix$operator"] = FunctionSetType(mutableSetOf(
                    FunctionType(IntType, listOf(IntType, IntType), true),
                    FunctionType(Int16Type, listOf(Int16Type, Int16Type), true)))
        }

        for (operator in listOf(">", ">=", "<", "<=", "==", "!=")) {
            typeEnvironment["infix$operator"] = FunctionSetType(mutableSetOf(
                    FunctionType(BoolType, listOf(IntType, IntType), true),
                    FunctionType(BoolType, listOf(Int16Type, Int16Type), true)))
        }

        for (operator in listOf("&&", "||", "==", "!=")) {
            typeEnvironment["infix$operator"] = FunctionType(BoolType, listOf(BoolType, BoolType), true)
        }

        for (operator in listOf("+", "-", "<<", ">>", "~")) {
            typeEnvironment["prefix$operator"] = FunctionSetType(mutableSetOf(
                    FunctionType(IntType, listOf(IntType), true),
                    FunctionType(Int16Type, listOf(Int16Type), true)))
        }

        typeEnvironment["prefix!"] = FunctionType(BoolType, listOf(BoolType), true)

        typeEnvironment["print"] = FunctionSetType(mutableSetOf(
                FunctionType(VoidType, listOf(IntType), true),
                FunctionType(VoidType, listOf(Int16Type), true),
                FunctionType(VoidType, listOf(StringType), true)))

        typeEnvironment["println"] = FunctionSetType(mutableSetOf(
                FunctionType(VoidType, listOf(IntType), true),
                FunctionType(VoidType, listOf(Int16Type), true),
                FunctionType(VoidType, listOf(StringType), true)))

        typeEnvironment
    }
}
