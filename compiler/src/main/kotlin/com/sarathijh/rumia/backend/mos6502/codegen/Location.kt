package com.sarathijh.rumia.backend.mos6502.codegen

sealed class Location(val bytes: Int = 1, var memberOffset: Int = 0) {
    open fun nextByte(): Location {
        throw Exception("No next byte")
    }
}

object NoLocation : Location(0)

object A : Location(1)
object X : Location(1)
object Y : Location(1)

data class ImmediateLocation(val value: Int) : Location(if (value > 255) 2 else 1) {
    override fun nextByte() = ImmediateLocation(value shr 8)
}

open class Address(var value: Int, bytes: Int = 1) : Location(bytes) {

    override fun nextByte(): Location {
        if (bytes == 1) {
            throw Exception("Address has no more bytes")
        }
        return Address(value + 1, bytes - 1)
    }

    fun isZeroPage() = (value <= 0xFF)

    fun immediateAddress() = ImmediateLocation(value)

    fun offset(amount: Int) {
        value += amount
    }

    override fun equals(other: Any?): Boolean {
        return when (other) {
            is Address -> ((this.value == other.value) && (this.bytes == other.bytes))
            else -> false
        }
    }
}

class IndirectLocation(value: Int, bytes: Int, val index: Location? = null) : Address(value, bytes) {
    init {
        if (!isZeroPage()) {
            throw Exception("Indirect address must be stored on zero page")
        }
    }

    override fun nextByte() = this

    fun toAbsolute() = Address(value, 2)

    fun atIndex(indexLocation: Location) = IndirectLocation(value, bytes, indexLocation)

    override fun equals(other: Any?): Boolean {
        if (other is IndirectLocation) {
            return value == other.value && bytes == other.bytes && index == other.index
        }
        return false
    }
}

class LabelLocation(val label: String, bytes: Int = 0, val offset: Int = 0) : Location(bytes) {
    override fun nextByte(): Location {
        return LabelLocation(label, bytes - 1, offset + 1)
    }
}

class ImmediateLabelLocation(val label: String, val byte: Int = 0) : Location(2 - byte) {
    override fun nextByte(): Location {
        return ImmediateLabelLocation(label, byte + 1)
    }
}

class SubroutineLocation(val label: String, val codeGenEnvironment: CodeGenEnvironment, val returnLocation: Location, val parameterNames: List<String>) : Location(2)
