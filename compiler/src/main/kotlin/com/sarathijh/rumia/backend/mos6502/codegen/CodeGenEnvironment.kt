package com.sarathijh.rumia.backend.mos6502.codegen

import com.sarathijh.rumia.frontend.typecheck.ArrayType
import com.sarathijh.rumia.frontend.typecheck.ReferenceType
import com.sarathijh.rumia.frontend.typecheck.Type
import java.util.*

class CodeGenEnvironment(private val parent: CodeGenEnvironment? = null) {

    companion object {
        val SUBROUTINE_RETURN_ADDRESS = Address(0x0, 2)
        val HEAP_ADDRESS_SPACE = 0x1000 until 0x2400
    }

    private val childEnvironments = mutableListOf<CodeGenEnvironment>()
    private var heapPointer: Int = 0
    private var bytesAllocated: Int = 0

    private var tempAddressPool = PriorityQueue<Int>()

    private var constantStringId = 0
    private val constantStringLabels = mutableMapOf<String, String>()
    private var variablesAddresses = mutableMapOf<String, Address>()
    private var subroutineAddresses = mutableMapOf<String, SubroutineLocation>()

    private val tempRange = IntRange(0x50, 0xFF)

    init {
        tempAddressPool.addAll(tempRange)
    }

    fun commitAllocations() {
        heapPointer = if (parent != null) {
            parent.heapPointer + parent.bytesAllocated
        } else {
            0x10
        }
        variablesAddresses.forEach { it.value.offset(heapPointer) }
        childEnvironments.forEach { it.commitAllocations() }
    }

    fun allocateConstantString(string: String) {
        val label = "constantString$constantStringId"
        constantStringId += 1
        constantStringLabels[string] = label
    }

    fun lookupConstantStringLabel(string: String) = constantStringLabels[string]!!

    fun getConstantStringMap(): Map<String, List<Int>> {
        return constantStringLabels.map { (stringValue, label) -> label to stringValue.toByteArray().map { it.toInt() } }.toMap()
    }

    fun allocateSubroutine(name: String, parameterNames: List<String>, parameterTypes: List<Type>, returnLocation: Location, subroutineCodeGenEnvironment: CodeGenEnvironment): Location {
        val label = mangleFunctionNameWithParameterTypes(name, parameterTypes)
        val address = SubroutineLocation(label, subroutineCodeGenEnvironment, returnLocation, parameterNames)

        subroutineAddresses[label] = address

        return address
    }

    fun allocateLocalVariable(name: String, type: Type): Location {
        val address = when (type) {
            is ReferenceType -> IndirectLocation(bytesAllocated, type.innerType.bytes)
            is ArrayType -> IndirectLocation(bytesAllocated, type.elementType.bytes)
            else -> Address(bytesAllocated, type.bytes)
        }

        variablesAddresses[name] = address
        bytesAllocated += when (type) {
            is ReferenceType -> 2
            is ArrayType -> 2
            else -> address.bytes
        }

        return address
    }

    fun defineLocalVariableAddress(name: String, address: Address) {
        variablesAddresses[name] = address
    }

    fun mangleFunctionNameWithParameterTypes(name: String, parameterTypes: List<Type>): String {
        var label = "subroutine_$name"
        for (type in parameterTypes) {
            label += "_$type"
        }
        return label
    }

    fun lookupSubroutineAddress(label: String): SubroutineLocation {
        return when {
            subroutineAddresses.containsKey(label) -> subroutineAddresses[label]!!
            parent != null -> parent.lookupSubroutineAddress(label)
            else -> throw Exception("Could not find subroutine '$label'")
        }
    }

    fun lookupVariableAddress(name: String): Location {
        return when {
            variablesAddresses.containsKey(name) -> variablesAddresses[name]!!
            parent != null -> parent.lookupVariableAddress(name)
            else -> throw Exception("Could not find variable '$name'")
        }
    }

    fun generateTempAddress(bytes: Int = 1): Address {
        val addressValue = tempAddressPool.remove()
        for (i in 0 until bytes-1) {
            tempAddressPool.remove()
        }
        return Address(addressValue, bytes)
    }

    fun releaseTempAddress(location: Location) {
        if (location is Address && location.value in tempRange) {
            for (i in 0 until location.bytes) {
                tempAddressPool.add(location.value + i)
            }
        }
    }

    fun createChildEnvironment(): CodeGenEnvironment {
        val childEnvironment = CodeGenEnvironment(parent = this)
        childEnvironments.add(childEnvironment)
        return childEnvironment
    }
}
