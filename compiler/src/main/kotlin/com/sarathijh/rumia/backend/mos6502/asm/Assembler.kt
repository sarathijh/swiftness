package com.sarathijh.rumia.backend.mos6502.asm

import java.io.OutputStream

class Assembler(private val outputStream: OutputStream) {

    fun assemble(statements: List<AsmStatement>) = outputMachineCode(statements, findLabelLocations(statements))

    private fun findLabelLocations(statements: List<AsmStatement>): Map<String, Int> {
        var byteOffset = PROGRAM_CODE_BYTE_OFFSET
        val labelLocations = mutableMapOf<String, Int>()
        for (statement in statements) {
            when (statement) {
                is AsmInstruction -> byteOffset += OP_CODE_BYTE_COUNT + statement.instruction.parameterByteCount
                is AsmData -> byteOffset += statement.bytes.size
                is AsmLabel -> labelLocations[statement.label] = byteOffset
            }
        }
        return labelLocations
    }

    private fun outputMachineCode(statements: List<AsmStatement>, labelLocations: Map<String, Int>) {
        var byteOffset = PROGRAM_CODE_BYTE_OFFSET
        for (statement in statements) {
            when (statement) {

                is AsmInstruction -> {
                    byteOffset += 1 + statement.instruction.parameterByteCount

                    outputStream.write(statement.instruction.opCode.code)

                    val parameter = statement.parameter
                    when (parameter) {
                        is ByteParameter -> outputStream.writeByte(parameter.byte)
                        is WordParameter -> outputStream.writeWord(parameter.word)
                        is LabelParameter -> outputStream.writeWord(labelLocations[parameter.label]!! + parameter.offset)
                        is LabelAddressParameter -> outputStream.writeByte(labelLocations[parameter.label]!! shr (8*parameter.byte))
                        is RelativeLabelParameter -> {
                            val offset = labelLocations[parameter.label]!! - byteOffset
                            if (offset > RELATIVE_OFFSET_MAX_BYTES_FORWARD || offset < RELATIVE_OFFSET_MAX_BYTES_BACKWARD) {
                                throw Exception("Branch target '${parameter.label}' has offset $offset from branch. Must be in range [$RELATIVE_OFFSET_MAX_BYTES_BACKWARD, $RELATIVE_OFFSET_MAX_BYTES_FORWARD].")
                            }
                            outputStream.writeByte(offset)
                        }
                    }
                }

                is AsmData -> {
                    byteOffset += statement.bytes.size
                    statement.bytes.forEach(outputStream::write)
                }
            }
        }
    }

    private fun OutputStream.writeByte(byte: Int) = write(byte and 0xFF)

    private fun OutputStream.writeWord(word: Int) {
        writeByte(word)
        writeByte(word shr 8)
    }

    companion object {
        private const val PROGRAM_CODE_BYTE_OFFSET = 0x8000
        private const val OP_CODE_BYTE_COUNT = 1
        private const val RELATIVE_OFFSET_MAX_BYTES_FORWARD = 127
        private const val RELATIVE_OFFSET_MAX_BYTES_BACKWARD = -128
    }
}
