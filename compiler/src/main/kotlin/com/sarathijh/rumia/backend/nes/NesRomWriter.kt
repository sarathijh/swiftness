package com.sarathijh.rumia.backend.nes

import java.io.File
import java.io.OutputStream
import kotlin.math.ceil

class NesRomWriter(private val outputStream: OutputStream) {
    fun write(programRomData: ByteArray) {
        writeHeader(ceil(0x8000.toDouble() / 0x4000).toByte())
        outputStream.write(programRomData)
        for (i in 0x8000 + programRomData.size until 0xFFFA) {
            outputStream.write(0x00)
        }
        outputStream.write(0x00)
        outputStream.write(0x80)
        outputStream.write(0x00)
        outputStream.write(0x80)
        outputStream.write(0x00)
        outputStream.write(0x00)

        outputStream.write(File("/Users/sarathi/dev/SwiftNess/res/TestGame/res/characterData").readBytes())
    }

    private fun writeHeader(programRomSizeInMultiplesOf16KB: Byte) {
        // "NES" followed by MS-DOS end-of-file
        outputStream.write(byteArrayOf('N'.toByte(), 'E'.toByte(), 'S'.toByte(), 0x1A))
        // Size of PRG ROM in 16 KB units
        outputStream.write(byteArrayOf(programRomSizeInMultiplesOf16KB))
        // Size of CHR ROM in 8 KB units (Value 0 means the board uses CHR RAM)
        outputStream.write(byteArrayOf(0x01))
        // Flags 6
        outputStream.write(byteArrayOf(0x01))
        // Flags 7
        outputStream.write(byteArrayOf(0x00))
        // Size of PRG RAM in 8 KB units (Value 0 infers 8 KB for compatibility; see PRG RAM circuit)
        outputStream.write(byteArrayOf(0x00))
        // Flags 9
        outputStream.write(byteArrayOf(0x00))
        // Flags 10 (unofficial)
        outputStream.write(byteArrayOf(0x00))
        // Zero filled
        outputStream.write(byteArrayOf(0x00, 0x00, 0x00, 0x00, 0x00))
    }
}
