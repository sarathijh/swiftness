package com.sarathijh.rumia.backend.mos6502.asm

import com.sarathijh.rumia.backend.mos6502.asm.AddressingMode.*
import java.io.PrintStream

fun PrintStream.printAsm(statements: List<AsmStatement>) = statements
        .map { it.string }
        .forEach(::println)

private val AsmStatement.string get() = when (this) {
    is AsmInstruction -> {
        val parameter = instruction.addressingMode.string(parameter.string)
        "  ${instruction.kind.name} $parameter"
    }

    is AsmLabel -> "$label:"

    is AsmData -> {
        val data = bytes.joinToString(" ") { String.format("$%02X", it and 0xFF) }
        "  .bytes $data"
    }
}

private fun AddressingMode.string(parameter: String) = when (this) {
    Immediate -> "#$parameter"
    Accumulator -> "A"
    ZeroPage -> parameter
    ZeroPageX -> "$parameter,X"
    ZeroPageY -> "$parameter,Y"
    Absolute -> parameter
    AbsoluteX -> "$parameter,X"
    AbsoluteY -> "$parameter,Y"
    Indirect -> "($parameter)"
    IndirectX -> "($parameter,X)"
    IndirectY -> "($parameter),Y"
    Relative -> parameter
    Implied -> ""
}

private val AsmParameter.string get() = when (this) {
    is ByteParameter -> String.format("$%02X", byte and 0xFF)
    is WordParameter -> String.format("$%04X", word and 0xFFFF)
    is LabelParameter -> if (offset == 0) label else "$label+$offset"
    is RelativeLabelParameter -> label
    is LabelAddressParameter -> "$label[$byte]"
    is ImpliedParameter -> ""
}
