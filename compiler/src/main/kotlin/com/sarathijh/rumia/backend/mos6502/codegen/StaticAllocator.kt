package com.sarathijh.rumia.backend.mos6502.codegen

import com.sarathijh.rumia.frontend.ast.*
import com.sarathijh.rumia.frontend.typecheck.*

// TODO: When allocating variables, make sure functions that call each other don't allocate in each other's space
class StaticAllocator(private val rootEnvironment: CodeGenEnvironment) {

    private var codeGenEnvironment = rootEnvironment

    fun allocate(node: AstNode) {
        internalAllocate(node)
        rootEnvironment.commitAllocations()
    }

    private fun internalAllocate(node: AstNode) {
        when (node) {
            is BlockStatement -> {
                node.statements.forEach { internalAllocate(it) }
            }

            is ExpressionStatement -> {
                internalAllocate(node.expression)
            }

            is AssignStatement -> {
                internalAllocate(node.valueExpression)
            }

            is IfStatement -> {
                val bodyEnvironment = codeGenEnvironment.createChildEnvironment()
                node.bodyStatement.codeGenEnvironment = bodyEnvironment
                inEnvironment(bodyEnvironment) {
                    internalAllocate(node.bodyStatement)
                }

                if (node.elseStatement != null) {
                    val elseEnvironment = codeGenEnvironment.createChildEnvironment()
                    when (node.elseStatement) {
                        is BlockStatement -> node.elseStatement.codeGenEnvironment = elseEnvironment
                        is IfStatement -> node.elseStatement.bodyStatement.codeGenEnvironment = elseEnvironment
                    }
                    inEnvironment(elseEnvironment) {
                        internalAllocate(node.elseStatement)
                    }
                }
            }

            is WhileStatement -> {
                val bodyEnvironment = codeGenEnvironment.createChildEnvironment()
                node.bodyStatement.codeGenEnvironment = bodyEnvironment
                inEnvironment(bodyEnvironment) {
                    internalAllocate(node.bodyStatement)
                }
            }

            is ForInStatement -> {
                val bodyEnvironment = codeGenEnvironment.createChildEnvironment()
                node.bodyStatement.codeGenEnvironment = bodyEnvironment
                inEnvironment(bodyEnvironment) {
                    codeGenEnvironment.allocateLocalVariable(node.identifierExpression.name, ReferenceType(node.identifierExpression.type))
                    internalAllocate(node.bodyStatement)
                }
            }

            is VariableDeclaration -> {
                if (node.valueExpression != null) {
                    internalAllocate(node.valueExpression)
                }

                codeGenEnvironment.allocateLocalVariable(node.identifierExpression.name, node.type)
            }

            is FunctionDeclaration -> {
                val functionEnvironment = codeGenEnvironment.createChildEnvironment()

                node.bodyStatement.codeGenEnvironment = functionEnvironment

                inEnvironment (functionEnvironment) {
                    node.parameters.forEach {
                        codeGenEnvironment.allocateLocalVariable(it.identifierExpression.name, it.type)
                    }
                    internalAllocate(node.bodyStatement)
                }

                codeGenEnvironment.allocateSubroutine(
                        node.identifierExpression.name,
                        node.parameters.map { it.identifierExpression.name },
                        node.parameters.map { it.type },
                        returnAddressForType(node.returnTypeAnnotation?.type),
                        functionEnvironment)
            }

            is FunctionCallExpression -> {
                node.arguments.forEach(::internalAllocate)
            }

            is MemberAccessExpression -> internalAllocate(node.objectExpression)

            is StringLiteralExpression -> {
                rootEnvironment.allocateConstantString(node.stringValue)
            }
        }
    }

    private fun returnAddressForType(type: Type?): Location {
        return if (type != null) {
            when (type) {
                is Int16Type, is StringType -> CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS
                is IntType, is BoolType -> A
                else -> throw Exception("Type '$type' not yet supported as function return type")
            }
        } else {
            NoLocation
        }
    }

    private fun <T> inEnvironment(otherCodeGenEnvironment: CodeGenEnvironment, function: () -> T): T {
        val originalEnvironment = codeGenEnvironment
        codeGenEnvironment = otherCodeGenEnvironment
        val result = function()
        codeGenEnvironment = originalEnvironment
        return result
    }
}
