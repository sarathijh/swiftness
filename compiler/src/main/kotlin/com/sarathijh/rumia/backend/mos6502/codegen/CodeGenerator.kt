package com.sarathijh.rumia.backend.mos6502.codegen

import com.sarathijh.rumia.backend.mos6502.asm.*
import com.sarathijh.rumia.frontend.ast.*
import com.sarathijh.rumia.frontend.typecheck.*
import java.lang.Math.sqrt

// TODO: Figure out a way to make when and where results are stored clearer
// TODO: Make it so indirect indexing can be used with instructions other than LDA, e.g. ADC
// TODO: Make it so location bytes can be accessed with subscripts
class CodeGenerator {

    private val rootEnvironment = CodeGenEnvironment()
    private var environment = rootEnvironment

    private val entryPointLabel = "__main"

    private var resultLocation: Location = A

    private var nextLabelId = 0

    private var isInBranchCondition = false

    private var currentSubroutineAddress: SubroutineLocation? = null

    private val globalAsmStatements = mutableListOf<AsmStatement>()
    private val functionAsmStatements = mutableListOf<AsmStatement>()
    private var currentAsmStatements = globalAsmStatements

    private var isAllocateUsed = false
    private var isFreeUsed = false
    private var isCopyBytesUsed = false


    fun generate(node: AstNode): List<AsmStatement> {
        StaticAllocator(rootEnvironment).allocate(node)

        //initializeNesPictureProcessingUnit()
        //loadNesPalette()
        //loadBackground()
        //gameLoop()

        generateAsmForNode(node)

        if (isAllocateUsed || isFreeUsed || isCopyBytesUsed) {
            if (isAllocateUsed) {
                generateAllocateSubroutine()
            }
            if (isFreeUsed) {
                generateFreeSubroutine()
            }
            if (isCopyBytesUsed) {
                generateCopyBytesSubroutine()
            }
        }

        val asmStatements = mutableListOf<AsmStatement>()
        if (functionAsmStatements.isNotEmpty()) {
            asmStatements.add(AsmInstruction(Instruction(InstructionKind.JMP, AddressingMode.Absolute), LabelParameter(entryPointLabel)))
            asmStatements.addAll(functionAsmStatements)
        }

        asmStatements.add(AsmLabel(entryPointLabel))

        if (isAllocateUsed || isFreeUsed || isCopyBytesUsed) {
            asmStatements.addAll(generateDynamicAllocatorInitializationStatements())
        }
        asmStatements.addAll(globalAsmStatements)

        /*asmStatements.add(AsmLabel("__paletteData"))
        asmStatements.add(AsmData(listOf(
                0x00, // Universal background color
                0x00, 0x00, 0x00, // Background palette 0
                0x00, // Unused
                0x00, 0x00, 0x00, // Background palette 1
                0x00, // Unused
                0x00, 0x00, 0x00, // Background palette 2
                0x00, // Unused
                0x04, 0x19, 0x24, // Background palette 3

                0x31, // Mirror
                0x06, 0x37, 0x02, // Sprite palette 0
                0x00, // Mirror
                0x00, 0x00, 0x00, // Sprite palette 1
                0x00, // Mirror
                0x00, 0x00, 0x00, // Sprite palette 2
                0x00, // Mirror
                0x00, 0x00, 0x00  // Sprite palette 3
        )))*/

        val constantStrings = rootEnvironment.getConstantStringMap()
        for ((label, bytes) in constantStrings) {
            asmStatements.add(AsmLabel(label))
            asmStatements.add(AsmData(listOf(bytes.size and 0xFF, (bytes.size shr 8) and 0xFF) + bytes))
        }

        return asmStatements
    }

    private fun initializeNesPictureProcessingUnit() {
        currentAsmStatements = globalAsmStatements

        A.load(0b00000000)
        A.store(Address(0x2000))

        A.load(0b00011110)
        A.store(Address(0x2001))
    }

    private fun loadNesPalette() {
        currentAsmStatements = globalAsmStatements

        A.load(0x3F)
        A.store(Address(0x2006))
        A.load(0x00)
        A.store(Address(0x2006))

        X.load(0)
        addLabel("__loadPalette")
        currentAsmStatements.add(AsmInstruction(Instruction(InstructionKind.LDA, AddressingMode.AbsoluteX), LabelParameter("__paletteData")))
        A.store(Address(0x2007))
        X.increment()
        addInstruction(InstructionKind.CPX, 0x20)
        addBranch(InstructionKind.BNE, "__loadPalette")
    }

    /*private fun loadBackground() {
        currentAsmStatements = globalAsmStatements

        A.load(0x20)
        A.store(Address(0x2006))
        A.store(Address(0x2006))

        X.load(0x00)
        addLabel("__loadBackgroundNames")
        A.load()
        lda ourMap, X ; load A with a byte from address (ourMap + X)
        inx
        sta $2007
        cpx #64 ; map in previous section 64 bytes long
        bne loadNames ; if not all 64 done, loop and do some more
    }*/

    private fun waitForVBlank() {
        currentAsmStatements = globalAsmStatements

        addLabel("__waitForVBlank")
        A.load(Address(0x2002))
        addBranch(InstructionKind.BPL, "__waitForVBlank")
    }

    private fun gameLoop() {
        currentAsmStatements = globalAsmStatements

        A.load(10)
        A.store(Address(0x10))
        A.load(18)
        A.store(Address(0x11))
        A.load(223 - 16)
        A.store(Address(0x12))
        A.load(231 - 16)
        A.store(Address(0x13))

        addLabel("__gameLoop")

        A.load(0x01)
        A.store(Address(0x4016))
        A.load(0x00)
        A.store(Address(0x4016))

        A.load(Address(0x4016))
        A.load(Address(0x4016))
        A.load(Address(0x4016))
        A.load(Address(0x4016))

        A.load(Address(0x4016))
        addInstruction(InstructionKind.AND, 1)
        addBranch(InstructionKind.BEQ, "__upNotPressed")
        Address(0x12).decrement()
        Address(0x13).decrement()
        Address(0x12).decrement()
        Address(0x13).decrement()
        addLabel("__upNotPressed")

        A.load(Address(0x4016))
        addInstruction(InstructionKind.AND, 1)
        addBranch(InstructionKind.BEQ, "__downNotPressed")
        Address(0x12).increment()
        Address(0x13).increment()
        Address(0x12).increment()
        Address(0x13).increment()
        addLabel("__downNotPressed")

        A.load(Address(0x4016))
        addInstruction(InstructionKind.AND, 1)
        addBranch(InstructionKind.BEQ, "__leftNotPressed")
        Address(0x10).decrement()
        Address(0x11).decrement()
        Address(0x10).decrement()
        Address(0x11).decrement()
        addLabel("__leftNotPressed")

        A.load(Address(0x4016))
        addInstruction(InstructionKind.AND, 1)
        addBranch(InstructionKind.BEQ, "__rightNotPressed")
        Address(0x10).increment()
        Address(0x11).increment()
        Address(0x10).increment()
        Address(0x11).increment()
        addLabel("__rightNotPressed")

        initializeOam()
        waitForVBlank()
        loadSprite(58, Address(0x10), Address(0x12))
        loadSprite(55, Address(0x11), Address(0x12))
        loadSprite(79, Address(0x10), Address(0x13))
        loadSprite(79, Address(0x11), Address(0x13), true)
        addJumpTo("__gameLoop")
    }

    private fun initializeOam() {
        currentAsmStatements = globalAsmStatements

        A.load(0)
        A.store(Address(0x2003))
        A.store(Address(0x2003))
    }

    private fun loadSprite(sprite: Int, x: Location, y: Location, flipped: Boolean = false) {
        currentAsmStatements = globalAsmStatements

        A.load(y)
        A.store(Address(0x2004))
        A.load(sprite)
        A.store(Address(0x2004))
        A.load(if (flipped) 0x40 else 0x00)
        A.store(Address(0x2004))
        A.load(x)
        A.store(Address(0x2004))
    }

    private fun loadBackground() {
        currentAsmStatements = globalAsmStatements

        A.load(0x20)
        A.store(Address(0x2006))
        A.load(0x00)
        A.store(Address(0x2006))

        X.load(0x04)
        Y.load(0xC0)
        A.load(133)
        addLabel("__loadNametable")
        A.store(Address(0x2007))
        Y.decrement()
        addBranch(InstructionKind.BNE, "__loadNametable")
        X.decrement()
        addBranch(InstructionKind.BNE, "__loadNametable")
    }

    private fun generateDynamicAllocatorInitializationStatements(): List<AsmStatement> {
        val statements = mutableListOf<AsmStatement>()

        currentAsmStatements = statements

        val heapStart = CodeGenEnvironment.HEAP_ADDRESS_SPACE.start
        val heapSize = CodeGenEnvironment.HEAP_ADDRESS_SPACE.count() - 2

        ImmediateLocation(heapSize and 0xFF).transferTo(Address(heapStart))
        ImmediateLocation((heapSize shr 8) and 0xFF).transferTo(Address(heapStart + 1))

        return statements
    }

    private fun generateAllocateSubroutine() {
        currentAsmStatements = functionAsmStatements

        val heapStart = CodeGenEnvironment.HEAP_ADDRESS_SPACE.start
        val heapSize = CodeGenEnvironment.HEAP_ADDRESS_SPACE.count()

        addLabel("__allocate")

        // currentBlock = heapStart
        A.load(heapStart and 0xFF)
        A.store(CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS)
        A.load((heapStart shr 8) and 0xFF)
        A.store(CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS.nextByte())

        addLabel("__allocateFreeSearchLoop")

        // blockSize = *currentBlock
        Y.load(0)
        A.load(IndirectLocation(CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS.value and 0xFF, 1))
        A.store(Address(2, 1))
        Y.load(1)
        A.load(IndirectLocation((CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS.value shr 8) and 0xFF, 1))
        A.store(Address(3, 1))

        // if (blockSize >= 0) goto __allocateBlockIsFree
        A.load(Address(3))
        addBranch(InstructionKind.BPL, "__allocateBlockIsFree")
        addJumpTo("__allocateBlockIsNotFree")
        addLabel("__allocateBlockIsFree")

        // if (blockSize >= bytes) goto __allocateFreeBlockIsLargeEnough
        performComparison(">=", Address(2, 2), Address(4, 2), "__allocateFreeBlockIsLargeEnough", "__allocateBlockIsNotLargeEnoughShim")

        addLabel("__allocateBlockIsNotLargeEnoughShim")
        addJumpTo("__allocateBlockIsNotLargeEnough")

        addLabel("__allocateFreeBlockIsLargeEnough")
        // blockSize -= bytes
        resultLocation = Address(2, 2)
        performBinaryMathOperation("-", Address(2, 2), Address(4, 2))

        // *currentBlock = -bytes
        Y.load(0)
        A.load(Address(4, 1))
        addInstruction(InstructionKind.EOR, 0xFF)
        A.mathOperation("+", ImmediateLocation(1))
        A.store(IndirectLocation(CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS.value and 0xFF, 1))
        Y.load(1)
        A.load(Address(5, 1))
        addInstruction(InstructionKind.EOR, 0xFF)
        A.mathOperation("+", ImmediateLocation(0), true)
        A.store(IndirectLocation((CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS.value shr 8) and 0xFF, 1))

        // if (blockSize >= 3) goto __allocateSplitBlock
        performComparison(">=", Address(2, 2), ImmediateLocation(3), "__allocateSplitBlock", "__allocateReturn")

        addLabel("__allocateSplitBlock")

        // bytes += 2
        resultLocation = Address(4, 2)
        performBinaryMathOperation("+", Address(4, 2), ImmediateLocation(2))
        // currentBlock += bytes
        CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS.transferTo(Address(6, 2))
        resultLocation = Address(6, 2)
        performBinaryMathOperation("+", Address(6, 2), Address(4, 2))

        // blockSize -= 2
        resultLocation = Address(2, 2)
        performBinaryMathOperation("-", Address(2, 2), ImmediateLocation(2))

        // *currentBlock = blockSize
        Y.load(0)
        A.load(Address(2, 1))
        A.store(IndirectLocation(6, 1))
        Y.load(1)
        A.load(Address(3, 1))
        A.store(IndirectLocation(6, 1))

        addLabel("__allocateReturn")

        // return currentBlock + 2
        resultLocation = CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS
        performBinaryMathOperation("+", CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS, ImmediateLocation(2))
        addInstruction(InstructionKind.RTS)

        addLabel("__allocateBlockIsNotFree")

        A.load(Address(2))
        addInstruction(InstructionKind.EOR, 0xFF)
        A.mathOperation("+", ImmediateLocation(1))
        A.store(Address(2))

        A.load(Address(3))
        addInstruction(InstructionKind.EOR, 0xFF)
        A.mathOperation("+", ImmediateLocation(0), true)
        A.store(Address(3))

        addLabel("__allocateBlockIsNotLargeEnough")

        resultLocation = CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS
        performBinaryMathOperation("+", CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS, Address(2, 2))
        resultLocation = CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS
        performBinaryMathOperation("+", CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS, ImmediateLocation(2))

        performComparison("<", CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS, ImmediateLocation(heapStart + heapSize), "__allocateFreeSearchLoopShim", "__allocateOutOfFreeMemory")

        addJumpTo("__allocateOutOfFreeMemory")
        addLabel("__allocateFreeSearchLoopShim")
        addJumpTo("__allocateFreeSearchLoop")

        addLabel("__allocateOutOfFreeMemory")
        addInstruction(InstructionKind.BRK)

        currentAsmStatements = globalAsmStatements
    }

    private fun generateFreeSubroutine() {
        currentAsmStatements = functionAsmStatements

        addLabel("__free")

        resultLocation = Address(0, 2)
        performBinaryMathOperation("-", resultLocation, ImmediateLocation(2))

        Y.load(0)
        A.load(IndirectLocation(0x00, 1))
        addInstruction(InstructionKind.EOR, 0xFF)
        A.mathOperation("+", ImmediateLocation(1))
        A.store(IndirectLocation(0x00, 1))

        Y.load(1)
        A.load(IndirectLocation(0x00, 1))
        addInstruction(InstructionKind.EOR, 0xFF)
        A.mathOperation("+", ImmediateLocation(0), true)
        A.store(IndirectLocation(0x00, 1))

        addInstruction(InstructionKind.RTS)
        currentAsmStatements = globalAsmStatements
    }

    private fun generateCopyBytesSubroutine() {
        currentAsmStatements = functionAsmStatements

        // function copyBytes(source: Address, destination: Address, numBytes: Int)
        // sourceAddress = (0x00, 0x01)
        // destinationAddress = (0x02, 0x03)

        addLabel("__copyBytes")

        // if (Y == 0) goto __copyBytesEnd
        A.load(Y)
        addBranch(InstructionKind.BEQ, "__copyBytesEnd")

        // destination[Y] = source[Y]
        A.load(ImmediateLocation(5))
        A.store(IndirectLocation(0x02, 1))

        // --Y
        addInstruction(InstructionKind.DEY)

        addJumpTo("__copyBytes")

        addLabel("__copyBytesEnd")
        addInstruction(InstructionKind.RTS)
        currentAsmStatements = globalAsmStatements
    }

    private fun copyBytes(source: Location, destination: Location, numBytes: Location): Location {
        isCopyBytesUsed = true

        source.transferAddressTo(Address(0, 2))
        destination.transferAddressTo(Address(2, 2))
        Y.load(numBytes)
        addInstruction(InstructionKind.JSR, LabelLocation("__copyBytes"))
        return NoLocation
    }

    private fun allocateMemory(numBytes: Location): Address {
        isAllocateUsed = true

        numBytes.transferTo(Address(0x04, 2))
        addInstruction(InstructionKind.JSR, LabelLocation("__allocate"))

        return CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS
    }

    private fun freeMemory(address: Address) {
        isFreeUsed = true

        address.immediateAddress().transferTo(Address(0x00, 2))
        addInstruction(InstructionKind.JSR, LabelLocation("__free"))
    }

    private fun generateAsmForNode(node: AstNode): Location {
        return when (node) {

            is BlockStatement -> {
                node.statements.forEach { generateAsmForNode(it) }
                NoLocation
            }

            is AstNodeList<*> -> NoLocation

            is ExpressionStatement -> generateAsmForNode(node.expression)

            is AssignStatement -> {
                val variableLocation = environment.lookupVariableAddress(node.identifierExpression.name)

                forResultLocationAs (variableLocation) {
                    val valueLocation = generateAsmForNode(node.valueExpression)
                    valueLocation.transferTo(variableLocation)
                }

                NoLocation
            }

            is SubscriptAssignStatement -> {
                val variableLocation = environment.lookupVariableAddress(node.subscriptExpression.identifierExpression.name) as IndirectLocation

                forResultLocationAs (variableLocation) {
                    val index = generateAsmForNode(node.subscriptExpression.indexExpression)
                    val valueLocation = generateAsmForNode(node.valueExpression)
                    valueLocation.memberOffset = 1
                    valueLocation.transferTo(variableLocation.atIndex(index))
                }

                NoLocation
            }

            is IfStatement -> {
                node.nextLabel = generateLabel("if_end")
                node.conditionExpression.trueLabel = generateLabel("if_true")
                node.conditionExpression.falseLabel =
                        if (node.elseStatement != null) {
                            generateLabel("if_false")
                        } else {
                            node.nextLabel
                        }

                node.bodyStatement.nextLabel = node.nextLabel
                node.elseStatement?.nextLabel = node.nextLabel

                branchCondition(node.conditionExpression)

                addLabel(node.conditionExpression.trueLabel)
                inEnvironment (node.bodyStatement.codeGenEnvironment!!) {
                    generateAsmForNode(node.bodyStatement)
                }

                if (node.elseStatement != null) {
                    addJumpTo(node.nextLabel)
                    addLabel(node.conditionExpression.falseLabel)
                    val environment = when (node.elseStatement) {
                        is BlockStatement -> node.elseStatement.codeGenEnvironment
                        is IfStatement -> node.bodyStatement.codeGenEnvironment
                        else -> throw Exception("Else statement has no code generation environment")
                    }
                    inEnvironment (environment!!) {
                        generateAsmForNode(node.elseStatement)
                    }
                }

                addLabel(node.nextLabel)

                NoLocation
            }

            is WhileStatement -> {
                val beginLabel = generateLabel("while_begin")
                node.nextLabel = generateLabel("while_end")
                node.conditionExpression.trueLabel = generateLabel("true")
                node.conditionExpression.falseLabel = node.nextLabel
                node.bodyStatement.nextLabel = beginLabel

                addLabel(beginLabel)

                isInBranchCondition = true
                generateAsmForNode(node.conditionExpression)
                isInBranchCondition = false

                addLabel(node.conditionExpression.trueLabel)
                inEnvironment (node.bodyStatement.codeGenEnvironment!!) {
                    generateAsmForNode(node.bodyStatement)
                }
                addJumpTo(beginLabel)
                addLabel(node.nextLabel)

                NoLocation
            }

            is ForInStatement -> {
                inEnvironment (node.bodyStatement.codeGenEnvironment!!) {
                    val elementAddress = environment.lookupVariableAddress(node.identifierExpression.name) as IndirectLocation

                    val arrayAddress = generateAsmForNode(node.arrayExpression) as IndirectLocation
                    val arrayType = node.arrayExpression.type as ArrayType

                    val beginLabel = generateLabel("for_begin")
                    val bodyLabel = generateLabel("for_body")
                    val endLabel = generateLabel("for_end")

                    val arrayLengthBytes = arrayType.size * arrayType.elementType.bytes

                    arrayAddress.toAbsolute().transferTo(elementAddress.toAbsolute())
                    Y.load(0)
                    addLabel(beginLabel)
                    performComparison("<", Y, ImmediateLocation(arrayLengthBytes + 1), bodyLabel, endLabel)
                    addLabel(bodyLabel)
                    generateAsmForNode(node.bodyStatement)
                    if (arrayType.elementType.bytes == 1) {
                        Y.increment()
                    } else {
                        performBinaryMathOperation("+", Y, ImmediateLocation(arrayType.elementType.bytes))
                    }
                    addJumpTo(beginLabel)
                    addLabel(endLabel)
                }

                NoLocation
            }

            is ReturnStatement -> {
                if (node.valueExpression != null) {
                    val returnAddress = currentSubroutineAddress!!.returnLocation
                    resultLocation = returnAddress

                    generateAsmForNode(node.valueExpression)
                            .transferTo(returnAddress)
                }

                currentAsmStatements.add(AsmInstruction(Instruction(InstructionKind.RTS, AddressingMode.Implied), ImpliedParameter))
                NoLocation
            }

            is VariableDeclaration -> {
                if (node.valueExpression != null) {
                    val variableLocation = environment.lookupVariableAddress(node.identifierExpression.name)

                    forResultLocationAs (variableLocation) {
                        resultLocation = variableLocation
                        val valueLocation = generateAsmForNode(node.valueExpression)
                        if (valueLocation is ImmediateLabelLocation) {
                            valueLocation.copyTo(variableLocation)
                        } else {
                            // TODO: Make the location byte count be the byte count for the pointer
                            // then add a method to get the pointer absolute address
                            if (variableLocation is IndirectLocation) {
                                valueLocation.transferTo(variableLocation.toAbsolute())
                            } else {
                                valueLocation.transferTo(variableLocation)
                            }
                        }
                    }
                } else if (node.type is ArrayType) {
                    val variableLocation = environment.lookupVariableAddress(node.identifierExpression.name) as IndirectLocation
                    val arrayLocation = allocateMemory(ImmediateLocation(node.type.bytes))
                    arrayLocation.transferTo(variableLocation.toAbsolute())
                }

                NoLocation
            }

            is FunctionDeclaration -> {
                val mangledName = environment.mangleFunctionNameWithParameterTypes(
                        node.identifierExpression.name,
                        node.parameters.map { it.type })

                val functionAddress = environment.lookupSubroutineAddress(mangledName)

                inSubroutineEnvironment (functionAddress) {
                    addLabel(functionAddress.label)

                    generateAsmForNode(node.bodyStatement)

                    if (node.returnType == null) {
                        addInstruction(InstructionKind.RTS)
                    }
                }

                NoLocation
            }

            is FunctionParameter -> NoLocation

            is IdentifierExpression -> {
                val variableAddress = environment.lookupVariableAddress(node.name)

                if (isInBranchCondition && node.type == BoolType) {
                    A.load(variableAddress)
                    addBranch(InstructionKind.BEQ, node.falseLabel)
                    addJumpTo(node.trueLabel)
                    NoLocation
                } else {
                    variableAddress
                }
            }

            is TypeNameExpression -> throw Exception("Unimplemented: using type name as an expression")

            is IntLiteralExpression -> ImmediateLocation(node.intValue)

            is BoolLiteralExpression -> {
                if (isInBranchCondition) {
                    addJumpTo(if (node.boolValue) node.trueLabel else node.falseLabel)
                    NoLocation
                } else {
                    ImmediateLocation(if (node.boolValue) 1 else 0)
                }
            }

            is StringLiteralExpression -> {
                val stringLabel = rootEnvironment.lookupConstantStringLabel(node.stringValue)
                ImmediateLabelLocation(stringLabel)
            }

            is ArrayLiteralExpression -> {
                val referenceLocation = resultLocation as IndirectLocation
                val arrayType = node.type as ArrayType
                val arrayLocation = allocateMemory(ImmediateLocation(arrayType.bytes + 1))
                arrayLocation.transferTo(referenceLocation.toAbsolute())

                Y.load(0)
                ImmediateLocation(arrayType.size).transferTo(referenceLocation)

                node.elementExpressions.forEach {
                    val elementLocation = generateAsmForNode(it)
                    Y.increment()
                    elementLocation.transferTo(referenceLocation)
                }

                referenceLocation.toAbsolute()
            }

            is FunctionCallExpression -> {
                when {
                    node.matchesSignature("println", listOf(IntType)) -> {
                        val argumentAddress = generateAsmForNode(node.arguments.first())

                        A.load(argumentAddress)
                        addInstruction(InstructionKind.SYS, 0x05)

                        A.load('\n'.toInt())
                        addInstruction(InstructionKind.SYS, 0x00)

                        NoLocation
                    }

                    node.matchesSignature("println", listOf(Int16Type)) -> {
                        forResultLocationAs (CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS) {
                            val argumentAddress = generateAsmForNode(node.arguments.first())
                            argumentAddress.transferTo(CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS)
                            addInstruction(InstructionKind.SYS, 0x06)
                        }

                        A.load('\n'.toInt())
                        addInstruction(InstructionKind.SYS, 0x00)

                        NoLocation
                    }

                    node.matchesSignature("println", listOf(StringType)) -> {
                        val argumentAddress = generateAsmForNode(node.arguments.first())

                        argumentAddress.transferTo(CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS)
                        addInstruction(InstructionKind.SYS, 0x01)

                        A.load('\n'.toInt())
                        addInstruction(InstructionKind.SYS, 0x00)

                        NoLocation
                    }

                    node.matchesSignature("print", listOf(IntType)) -> {
                        val argumentAddress = generateAsmForNode(node.arguments.first())

                        A.load(argumentAddress)
                        addInstruction(InstructionKind.SYS, 0x05)

                        NoLocation
                    }

                    node.matchesSignature("print", listOf(Int16Type)) -> {
                        forResultLocationAs (CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS) {
                            val argumentAddress = generateAsmForNode(node.arguments.first())
                            argumentAddress.transferTo(CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS)
                            addInstruction(InstructionKind.SYS, 0x06)
                        }

                        NoLocation
                    }

                    node.matchesSignature("print", listOf(StringType)) -> {
                        val argumentAddress = generateAsmForNode(node.arguments.first())

                        argumentAddress.transferTo(CodeGenEnvironment.SUBROUTINE_RETURN_ADDRESS)
                        addInstruction(InstructionKind.SYS, 0x01)

                        NoLocation
                    }

                    else -> {
                        val mangledName = environment.mangleFunctionNameWithParameterTypes(
                                node.identifierExpression.name,
                                node.functionType.parameterTypes)

                        val functionAddress = environment.lookupSubroutineAddress(mangledName)

                        val parameterAddresses = inEnvironment (functionAddress.codeGenEnvironment) {
                            functionAddress.parameterNames.map(environment::lookupVariableAddress)
                        }

                        node.arguments.zip(parameterAddresses).forEach { (argument, parameterAddress) ->
                            forResultLocationAs (parameterAddress) {
                                val argumentAddress = generateAsmForNode(argument)
                                argumentAddress.transferTo(parameterAddress)
                            }
                        }

                        addInstruction(InstructionKind.JSR, functionAddress)

                        functionAddress.returnLocation
                    }
                }
            }

            is MemberAccessExpression -> {
                val memberName = node.memberExpression.name

                when (node.objectExpression.type) {

                    is IntClassType -> {
                        when (memberName) {
                            "min" -> return ImmediateLocation(0)
                            "max" -> return ImmediateLocation(0xFF)
                        }
                    }

                    is Int16ClassType -> {
                        when (memberName) {
                            "min" -> return ImmediateLocation(0)
                            "max" -> return ImmediateLocation(0xFFFF)
                        }
                    }

                    is ArrayType -> {
                        val objectAddress = generateAsmForNode(node.objectExpression) as Address

                        when (memberName) {
                            "length" -> return IndirectLocation(objectAddress.value, 1)
                        }
                    }

                    is StringType -> {
                        val objectAddress = generateAsmForNode(node.objectExpression) as Address

                        when (memberName) {
                            "length" -> return IndirectLocation(objectAddress.value, 2)
                        }
                    }
                }

                throw Exception("Unimplemented: member access for non-array types")
            }

            is SubscriptExpression -> {
                val variableLocation = environment.lookupVariableAddress(node.identifierExpression.name) as IndirectLocation
                val index = generateAsmForNode(node.indexExpression)

                val result = variableLocation.atIndex(index)
                result.memberOffset = 1
                result
            }

            is InfixExpression -> {
                when (node.operator) {
                    "+", "-", "&", "|", "^", "%", "*" -> {
                        val targetLocation = if (node.type.bytes == 1) A else resultLocation
                        forResultLocationAs(targetLocation) {
                            val rightAddress = storeInTempAddress(generateAsmForNode(node.rightExpression))
                            val leftAddress = generateAsmForNode(node.leftExpression)

                            performBinaryMathOperation(node.operator, leftAddress, rightAddress)
                            environment.releaseTempAddress(rightAddress)
                        }
                        return targetLocation
                    }

                    "==", "!=", "<", ">", ">=", "<=" -> {
                        if (isInBranchCondition) {
                            val leftAddress = generateAsmForNode(node.leftExpression)
                            val rightAddress = generateAsmForNode(node.rightExpression)

                            performComparison(node.operator, leftAddress, rightAddress, node.trueLabel, node.falseLabel)

                            return NoLocation
                        } else {
                            val leftAddress = generateAsmForNode(node.leftExpression)
                            val rightAddress = generateAsmForNode(node.rightExpression)

                            node.trueLabel = generateLabel("boolValueTrue")
                            node.falseLabel = generateLabel("boolValueFalse")
                            val doneLabel = generateLabel("boolValueDone")

                            performComparison(node.operator, leftAddress, rightAddress, node.trueLabel, node.falseLabel)

                            addLabel(node.trueLabel)
                            A.load(ImmediateLocation(1))
                            addJumpTo(doneLabel)
                            addLabel(node.falseLabel)
                            A.load(ImmediateLocation(0))
                            addLabel(doneLabel)

                            return A
                        }
                    }

                    "&&" -> {
                        if (isInBranchCondition) {
                            node.leftExpression.trueLabel = "true_$nextLabelId"
                            node.leftExpression.falseLabel = node.falseLabel
                            node.rightExpression.trueLabel = node.trueLabel
                            node.rightExpression.falseLabel = node.falseLabel
                            nextLabelId += 1

                            generateAsmForNode(node.leftExpression)
                            addLabel(node.leftExpression.trueLabel)
                            generateAsmForNode(node.rightExpression)

                            return NoLocation
                        } else {
                            val leftAddress = storeInTempAddress(generateAsmForNode(node.leftExpression))
                            val rightAddress = storeInTempAddress(generateAsmForNode(node.rightExpression))

                            A.load(leftAddress)
                            A.mathOperation("&", rightAddress)

                            environment.releaseTempAddress(leftAddress)
                            environment.releaseTempAddress(rightAddress)

                            return A
                        }
                    }

                    "||" -> {
                        if (isInBranchCondition) {
                            node.leftExpression.trueLabel = node.trueLabel
                            node.leftExpression.falseLabel = "false_$nextLabelId"
                            node.rightExpression.trueLabel = node.trueLabel
                            node.rightExpression.falseLabel = node.falseLabel
                            nextLabelId += 1

                            generateAsmForNode(node.leftExpression)
                            addLabel(node.leftExpression.falseLabel)
                            generateAsmForNode(node.rightExpression)

                            return NoLocation
                        } else {
                            val leftAddress = storeInTempAddress(generateAsmForNode(node.leftExpression))
                            val rightAddress = storeInTempAddress(generateAsmForNode(node.rightExpression))

                            A.load(leftAddress)
                            A.mathOperation("|", rightAddress)

                            environment.releaseTempAddress(leftAddress)
                            environment.releaseTempAddress(rightAddress)

                            return A
                        }
                    }

                    else -> throw Exception("Not implemented: code generation for operator '${node.operator}'")
                }
            }

            is PrefixExpression -> {
                val resultAddress = resultLocation

                when (node.operator) {
                    "+" -> generateAsmForNode(node.subExpression)

                    "~" -> {
                        val valueAddress = generateAsmForNode(node.subExpression)
                        pairBytesLittleEndian(valueAddress, resultAddress).forEach { (valueByte, resultByte) ->
                            A.load(valueByte)
                            addInstruction(InstructionKind.EOR, 0xFF)
                            A.store(resultByte)
                        }
                        return resultAddress
                    }

                    "!" -> {
                        return if (isInBranchCondition) {
                            node.subExpression.trueLabel = node.falseLabel
                            node.subExpression.falseLabel = node.trueLabel
                            generateAsmForNode(node.subExpression)
                            NoLocation
                        } else {
                            val valueAddress = generateAsmForNode(node.subExpression)
                            A.load(valueAddress)
                            // Booleans must always be either 0 or 1 for this to be correct!
                            addInstruction(InstructionKind.EOR, 1)
                            A
                        }
                    }

                    "<<", ">>" -> {
                        val targetLocation = if (node.type.bytes == 1) A else resultLocation

                        forResultLocationAs (targetLocation) {
                            val operandLocation = generateAsmForNode(node.subExpression)
                            performUnaryMathOperation(node.operator, operandLocation)
                        }

                        return targetLocation
                    }
                }

                val valueAddress = generateAsmForNode(node.subExpression)

                when (node.subExpression.type) {
                    IntType -> {
                        when (node.operator) {
                            "-" -> {
                                A.load(valueAddress)
                                addInstruction(InstructionKind.EOR, 0xFF)
                                clearCarry()
                                addInstruction(InstructionKind.ADC, 1)
                                A
                            }

                            else -> throw Exception("Unsupported prefix operator '${node.operator}'")
                        }
                    }

                    Int16Type -> {
                        when (node.operator) {
                            "-" -> {
                                A.load(valueAddress)
                                addInstruction(InstructionKind.EOR, 0xFF)
                                clearCarry()
                                addInstruction(InstructionKind.ADC, 1)
                                A.store(resultAddress)
                                A.load(valueAddress.nextByte())
                                addInstruction(InstructionKind.EOR, 0xFF)
                                addInstruction(InstructionKind.ADC, 1)
                                A.store(resultAddress.nextByte())
                                resultAddress
                            }

                            else -> throw Exception("Unsupported prefix operator '${node.operator}'")
                        }
                    }

                    BoolType -> {
                        when (node.operator) {
                            "!" -> {
                                val temp = node.subExpression.falseLabel
                                node.subExpression.falseLabel = node.subExpression.trueLabel
                                node.subExpression.trueLabel = temp
                                generateAsmForNode(node.subExpression)
                            }

                            else -> throw Exception("Unsupported prefix operator '${node.operator}'")
                        }
                    }

                    else -> throw Exception("Operator '${node.operator}' cannot be used on type '${node.subExpression.type}'")
                }
            }

            is TypeAnnotation -> throw Exception("Cannot generate code for a type annotation")
        }
    }

    //region Accumulator Helper Functions

    private fun A.load(immediateValue: Int) = A.load(ImmediateLocation(immediateValue))

    private fun A.load(location: Location) {
        when (location) {
            is A -> {}
            is X -> addInstruction(InstructionKind.TXA)
            is Y -> addInstruction(InstructionKind.TYA)
            else -> {
                /*if (location.bytes > 1) {
                    throw Exception("Cannot store ${location.bytes} bytes in A register")
                }*/

                if (location is IndirectLocation && location.index != null) {
                    // TODO: Handle case where index is more than 1 byte
                    Y.load(location.index)
                    if (location.memberOffset > 5) {
                        val prevResult = resultLocation
                        resultLocation = Y
                        performBinaryMathOperation("+", Y, ImmediateLocation(location.memberOffset))
                        resultLocation = prevResult
                    } else {
                        for (i in 0 until location.memberOffset) {
                            Y.increment()
                        }
                    }
                }

                addInstruction(InstructionKind.LDA, location)
            }
        }
    }

    private fun A.store(location: Location) {
        when (location) {
            is A -> {}
            is X -> addInstruction(InstructionKind.TAX)
            is Y -> addInstruction(InstructionKind.TAY)
            is ImmediateLocation -> throw Exception("Cannot store Accumulator at immediate address")
            else -> {
                if (location is IndirectLocation && location.index != null) {
                    Y.load(location.index)
                    if (location.memberOffset > 5) {
                        val prevResult = resultLocation
                        resultLocation = Y
                        performBinaryMathOperation("+", Y, ImmediateLocation(location.memberOffset))
                        resultLocation = prevResult
                    } else {
                        for (i in 0 until location.memberOffset) {
                            Y.increment()
                        }
                    }
                }
                addInstruction(InstructionKind.STA, location)
            }
        }
    }

    private fun A.mathOperation(operator: String, valueLocation: Location, leaveCarry: Boolean = false) {

        if (valueLocation is IndirectLocation && valueLocation.index != null) {
            // TODO: Handle case where index is more than 1 byte
            Y.load(valueLocation.index)
            if (valueLocation.memberOffset > 5) {
                val prevResult = resultLocation
                resultLocation = Y
                performBinaryMathOperation("+", Y, ImmediateLocation(valueLocation.memberOffset))
                resultLocation = prevResult
            } else {
                for (i in 0 until valueLocation.memberOffset) {
                    Y.increment()
                }
            }
        }

        when (operator) {
            "+" -> {
                if (!leaveCarry) { clearCarry() }
                addInstruction(InstructionKind.ADC, valueLocation)
            }
            "-" -> {
                if (!leaveCarry) { setCarry() }
                addInstruction(InstructionKind.SBC, valueLocation)
            }
            "&" -> addInstruction(InstructionKind.AND, valueLocation)
            "|" -> addInstruction(InstructionKind.ORA, valueLocation)
            "^" -> addInstruction(InstructionKind.EOR, valueLocation)
        }
    }

    //endregion

    //region X Helper Functions

    private fun X.load(immediateValue: Int) = X.load(ImmediateLocation(immediateValue))

    private fun X.load(location: Location) {
        when (location) {
            is X -> {}
            is Y -> throw Exception("Cannot transfer Y to X")
            is A -> addInstruction(InstructionKind.TAX)
            else -> {
                /*if (location.bytes > 1) {
                    throw Exception("Cannot store ${location.bytes} bytes in X register")
                }*/

                addInstruction(InstructionKind.LDX, location)
            }
        }
    }

    private fun X.store(location: Location) {
        when (location) {
            is X -> {}
            is Y -> throw Exception("Cannot transfer X to Y")
            is A -> addInstruction(InstructionKind.TXA)
            is ImmediateLocation -> throw Exception("Cannot store X register in immediate location")
            is IndirectLocation -> throw Exception("Cannot store X register in indirect location")
            else -> addInstruction(InstructionKind.STX, location)
        }
    }

    //endregion

    //region Y Helper Functions

    private fun Y.load(immediateValue: Int) = Y.load(ImmediateLocation(immediateValue))

    private fun Y.load(location: Location) {
        when (location) {
            is Y -> {}
            is X -> throw Exception("Cannot transfer X to Y")
            is A -> addInstruction(InstructionKind.TAY)
            else -> {
                /*if (location.bytes > 1) {
                    throw Exception("Cannot store ${location.bytes} bytes in Y register")
                }*/

                addInstruction(InstructionKind.LDY, location)
            }
        }
    }

    private fun Y.store(location: Location) {
        when (location) {
            is Y -> {}
            is X -> throw Exception("Cannot transfer Y to X")
            is A -> addInstruction(InstructionKind.TYA)
            is ImmediateLocation -> throw Exception("Cannot store Y register in immediate location")
            is IndirectLocation -> throw Exception("Cannot store Y register in indirect location")
            else -> addInstruction(InstructionKind.STY, location)
        }
    }

    //endregion

    //region Operator Helper Functions

    private fun performUnaryMathOperation(operator: String, operand: Location) {
        val result = resultLocation
        if (result.bytes < operand.bytes) {
            throw Exception("Trying to store math operation result of ${operand.bytes} bytes in address with ${result.bytes} bytes")
        }

        when (operator) {
            "<<", ">>" -> pairBytesLittleEndian(operand, result).forEachIndexed { i, (valueByte, resultByte) ->
                val instruction = when (operator) {
                    "<<" -> if (i == 0) InstructionKind.ASL else InstructionKind.ROL
                    ">>" -> if (i == 0) InstructionKind.LSR else InstructionKind.ROR
                    else -> throw Exception("unknown operator: '$operator'")
                }

                A.load(valueByte)
                addInstruction(instruction, AddressingMode.Accumulator)
                A.store(resultByte)
            }

            /*"-" -> {
                val resultTarget = resultLocation

                operand.pairBytesLittleEndian(resultTarget).forEach { (byte1, byte2) ->
                    Accumulator.load(byte1)
                    addInstruction(InstructionKind.EOR, 0xFF)
                    Accumulator.mathOperation("+", ImmediateLocation(1))
                    Accumulator.store(byte2)
                }

                resultTarget
            }*/

            else -> throw Exception("Unimplemented: unary math operation '$operator'")
        }
    }

    private fun performBinaryMathOperation(operator: String, left: Location, right: Location) {
        val result = resultLocation
        if (result.bytes < left.bytes || result.bytes < right.bytes) {
            throw Exception("Trying to store math operation result of ${Integer.max(left.bytes, right.bytes)} bytes in address with ${result.bytes} bytes")
        }

        when (operator) {
            "+", "-", "&", "|", "^" -> pairBytesLittleEndian(left, right, result).forEachIndexed { i, (leftByte, rightByte, resultByte) ->
                A.load(leftByte)
                A.mathOperation(operator, rightByte, i != 0)
                A.store(resultByte)
            }

            "%" -> {
                // TODO: Do this in the optimizer? Just replace '%' operation with '&' and subtract 1 from rhs if it's a power of 2
                if (right is ImmediateLocation && right.value.isPowerOf2()) {
                    A.load(left)
                    addInstruction(InstructionKind.AND, right.value - 1)
                } else {
                    // TODO: Add power of two optimization check for non-compile time constant values
                    val beginLabel = generateLabel("modBegin")
                    val divByZeroLabel = generateLabel("modDivByZero")
                    val noSubLabel = generateLabel("modNoSub")
                    val endLabel = generateLabel("modEnd")

                    A.load(right)
                    addBranch(InstructionKind.BEQ, divByZeroLabel)

                    val numerator = environment.generateTempAddress(1)
                    left.transferTo(numerator)

                    val remainder = environment.generateTempAddress(1)
                    remainder.loadZero()

                    X.load(ImmediateLocation(8))

                    addLabel(beginLabel)
                    addInstruction(InstructionKind.ASL, numerator)
                    addInstruction(InstructionKind.ROL, remainder)
                    addInstruction(InstructionKind.LDA, remainder)
                    addInstruction(InstructionKind.SEC)
                    addInstruction(InstructionKind.SBC, right)
                    addBranch(InstructionKind.BCC, noSubLabel)
                    addInstruction(InstructionKind.STA, remainder)
                    addLabel(noSubLabel)
                    addInstruction(InstructionKind.DEX)
                    addBranch(InstructionKind.BNE, beginLabel)
                    addJumpTo(endLabel)

                    addLabel(divByZeroLabel)
                    addInstruction(InstructionKind.BRK)
                    addLabel(endLabel)
                    addInstruction(InstructionKind.LDA, remainder)

                    environment.releaseTempAddress(remainder)
                    environment.releaseTempAddress(numerator)
                }
            }

            "*" -> {
                if (right is ImmediateLocation && right.value.isPowerOf2()) {
                    A.load(left)
                    for (i in 0 until sqrt(right.value.toDouble()).toInt()) {
                        addInstruction(InstructionKind.ASL, AddressingMode.Accumulator)
                    }
                    A.store(result)
                } else {
                    val leftAddress = storeInTempAddress(left)
                    val rightAddress = storeInTempAddress(right)

                    val loopLabel = generateLabel("mul_loop")
                    val noAddLabel = generateLabel("mul_no_add")

                    result.loadZero()

                    X.load(ImmediateLocation(0x8))
                    addLabel(loopLabel)
                    addInstruction(InstructionKind.LSR, leftAddress)
                    addBranch(InstructionKind.BCC, noAddLabel)

                    performBinaryMathOperation("+", result, rightAddress)

                    addLabel(noAddLabel)
                    addInstruction(InstructionKind.DEX)
                    addBranch(InstructionKind.BNE, loopLabel)

                    environment.releaseTempAddress(leftAddress)
                    environment.releaseTempAddress(rightAddress)
                }
            }
        }
    }

    private fun performComparison(operator: String, left: Location, right: Location, trueLabel: String, falseLabel: String) {
        when (operator) {
            "==" -> {
                pairBytesLittleEndian(left, right).forEach { (leftByte, rightByte) ->
                    A.load(leftByte)
                    addInstruction(InstructionKind.CMP, rightByte)
                    addBranch(InstructionKind.BNE, falseLabel)
                }
                addJumpTo(trueLabel)
            }

            "!=" -> {
                pairBytesLittleEndian(left, right).forEach { (leftByte, rightByte) ->
                    A.load(leftByte)
                    addInstruction(InstructionKind.CMP, rightByte)
                    addBranch(InstructionKind.BNE, trueLabel)
                }
                addJumpTo(falseLabel)
            }

            "<", ">" -> {
                val bytePairs = if (operator == "<") {
                    pairBytesBigEndian(left, right)
                } else {
                    pairBytesBigEndian(right, left)
                }

                bytePairs.forEachIndexed { i, (leftByte, rightByte) ->
                    A.load(leftByte)
                    addInstruction(InstructionKind.CMP, rightByte)

                    if (i == bytePairs.size - 1) {
                        addBranch(InstructionKind.BCS, falseLabel)
                        addJumpTo(trueLabel)
                    } else {
                        val equalLabel = generateLabel("equal")

                        addBranch(InstructionKind.BCC, trueLabel)
                        addBranch(InstructionKind.BEQ, equalLabel)
                        addBranch(InstructionKind.BCS, falseLabel)

                        addLabel(equalLabel)
                    }
                }
            }

            ">=", "<=" -> {
                val bytePairs = if (operator == ">=") {
                    pairBytesBigEndian(left, right)
                } else {
                    pairBytesBigEndian(right, left)
                }

                bytePairs.forEachIndexed { i, (leftByte, rightByte) ->
                    A.load(leftByte)
                    addInstruction(InstructionKind.CMP, rightByte)

                    if (i == bytePairs.size - 1) {
                        addBranch(InstructionKind.BCS, trueLabel)
                        addJumpTo(falseLabel)
                    } else {
                        addBranch(InstructionKind.BCC, falseLabel)
                        addBranch(InstructionKind.BNE, trueLabel)
                    }
                }
            }
        }
    }

    //endregion

    //region Carry Helper Functions

    private fun clearCarry() = addInstruction(InstructionKind.CLC)
    private fun setCarry() = addInstruction(InstructionKind.SEC)

    //endregion

    //region Location Helper Functions

    private fun Location.loadZero() = ImmediateLocation(0).transferTo(this)

    private fun Location.increment() {
        when (this) {
            is X -> addInstruction(InstructionKind.INX)
            is Y -> addInstruction(InstructionKind.INY)
            is Address -> {
                addInstruction(InstructionKind.INC, this)
                var address = this
                for (i in 1 until this.bytes) {
                    address = address.nextByte()
                    val doneLabel = generateLabel("noincrement")
                    addBranch(InstructionKind.BNE, doneLabel)
                    addInstruction(InstructionKind.INC, address)
                    addLabel(doneLabel)
                }
            }
            else -> throw Exception("Incremented address must be an absolute address")
        }
    }

    private fun Location.decrement() {
        when (this) {
            is X -> addInstruction(InstructionKind.DEX)
            is Y -> addInstruction(InstructionKind.DEY)
            is Address -> {
                addInstruction(InstructionKind.DEC, this)
                var address = this
                for (i in 1 until this.bytes) {
                    address = address.nextByte()
                    val doneLabel = generateLabel("nodecrement")
                    addBranch(InstructionKind.BNE, doneLabel)
                    addInstruction(InstructionKind.DEC, address)
                    addLabel(doneLabel)
                }
            }
            else -> throw Exception("Decremented address must be an absolute address")
        }
    }

    private fun pairBytesBigEndian(vararg locations: Location) = pairBytesLittleEndian(*locations).reversed()

    private fun pairBytesLittleEndian(vararg locations: Location): List<Array<Location>> {
        val targetByteCount = locations.map { it.bytes }.max()!!
        val result = mutableListOf<Array<Location>>()

        var bytes = locations.map { it }.toTypedArray()
        for (i in 0 until targetByteCount) {
            result.add(bytes)

            if (i < targetByteCount - 1) {
                bytes = locations.map { it.nextByte() }.toTypedArray()
            }
        }

        return result
    }

    private fun Location.copyTo(to: Location) {
        val source = if (this is ImmediateLabelLocation) {
            LabelLocation(this.label, this.bytes, 0)
        } else {
            this
        }
        val destination = allocateMemory(source)
        destination.transferTo(to)
        copyBytes(source, destination, source)
    }

    private fun Location.transferTo(to: Location) {
        if (this == to) {
            return
        }

        if (this.bytes > to.bytes) {
            throw Exception("Trying to transfer value with ${this.bytes} bytes to address with ${to.bytes}")
        }

        var fromAddress = this
        var toAddress = to

        for (i in 0 until this.bytes) {
            A.load(fromAddress)
            A.store(toAddress)

            if (i < this.bytes - 1) {
                // TODO: Somehow increment the indices
                fromAddress = fromAddress.nextByte()
                toAddress = toAddress.nextByte()
            }
        }
    }

    private fun Location.transferAddressTo(to: Location) {
        val target = when (to) {
            is IndirectLocation -> to.toAbsolute()
            else -> to
        }
        when (this) {
            is Address -> this.immediateAddress().transferTo(target)
            is LabelLocation -> ImmediateLabelLocation(this.label).transferTo(target)
            is ImmediateLabelLocation -> this.transferTo(target)
            else -> throw Exception("Cannot transfer address of non-absolute address")
        }
    }

    //endregion

    //region Printing Helper Functions

    private fun addLabel(label: String) {
        currentAsmStatements.add(AsmLabel(label))
    }

    private fun addJumpTo(label: String) {
        currentAsmStatements.add(AsmInstruction(Instruction(InstructionKind.JMP, AddressingMode.Absolute), LabelParameter(label)))
    }

    private fun addBranch(kind: InstructionKind, label: String) {
        currentAsmStatements.add(AsmInstruction(Instruction(kind, AddressingMode.Relative), RelativeLabelParameter(label)))
    }

    private fun addInstruction(kind: InstructionKind, addressingMode: AddressingMode = AddressingMode.Implied) {
        currentAsmStatements.add(AsmInstruction(Instruction(kind, addressingMode), ImpliedParameter))
    }

    private fun addInstruction(kind: InstructionKind, immediateValue: Int) {
        addInstruction(kind, ImmediateLocation(immediateValue))
    }

    private fun addInstruction(kind: InstructionKind, parameterLocation: Location) {

        val addressingMode = when (parameterLocation) {
            NoLocation -> AddressingMode.Implied
            A -> AddressingMode.Implied
            X -> AddressingMode.Implied
            Y -> AddressingMode.Implied
            is ImmediateLocation -> AddressingMode.Immediate
            is IndirectLocation -> AddressingMode.IndirectY
            is Address -> if (parameterLocation.isZeroPage()) {
                AddressingMode.ZeroPage
            } else {
                AddressingMode.Absolute
            }
            is LabelLocation -> AddressingMode.Absolute
            is ImmediateLabelLocation -> AddressingMode.Immediate
            is SubroutineLocation -> AddressingMode.Absolute
        }

        val parameter = when(parameterLocation) {
            NoLocation -> ImpliedParameter
            A -> ImpliedParameter
            X -> ImpliedParameter
            Y -> ImpliedParameter
            is ImmediateLocation -> ByteParameter(parameterLocation.value and 0xFF)
            is IndirectLocation -> ByteParameter(parameterLocation.value and 0xFF)
            is Address -> if (parameterLocation.isZeroPage()) {
                ByteParameter(parameterLocation.value)
            } else {
                WordParameter(parameterLocation.value)
            }
            is LabelLocation -> LabelParameter(parameterLocation.label, parameterLocation.offset)
            is ImmediateLabelLocation -> LabelAddressParameter(parameterLocation.label, parameterLocation.byte)
            is SubroutineLocation -> LabelParameter(parameterLocation.label)
        }

        currentAsmStatements.add(AsmInstruction(Instruction(kind, addressingMode), parameter))
    }

    private fun generateLabel(prefix: String): String {
        val label = "${prefix}_$nextLabelId"
        nextLabelId += 1
        return label
    }

    //endregion

    private fun branchCondition(conditionExpression: Expression) {
        isInBranchCondition = true
        generateAsmForNode(conditionExpression)
        isInBranchCondition = false
    }

    private fun storeInTempAddress(location: Location): Location {
        return when (location) {
            is A -> {
                val tempAddress = environment.generateTempAddress()
                A.load(location)
                A.store(tempAddress)
                tempAddress
            }
            else -> location
        }
    }

    private fun <T> inEnvironment(otherCodeGenEnvironment: CodeGenEnvironment, function: () -> T): T {
        val originalEnvironment = environment
        environment = otherCodeGenEnvironment
        val result = function()
        environment = originalEnvironment
        return result
    }

    private fun forResultLocationAs(location: Location, function: () -> Unit) {
        val oldResultLocation = resultLocation
        resultLocation = location
        function()
        resultLocation = oldResultLocation
    }

    private fun inSubroutineEnvironment(location: SubroutineLocation, function: () -> Unit) {
        currentAsmStatements = functionAsmStatements
        val oldSubroutineLocation = currentSubroutineAddress
        currentSubroutineAddress = location

        inEnvironment(location.codeGenEnvironment) {
            function()
        }

        currentSubroutineAddress = oldSubroutineLocation
        currentAsmStatements = globalAsmStatements
    }

    private fun Int.isPowerOf2(): Boolean {
        return (this > 0 && ((this and (this - 1)) == 0))
    }

    private fun FunctionCallExpression.matchesSignature(name: String, parameterTypes: List<Type>): Boolean {
        return identifierExpression.name == name && functionType.parameterTypes == parameterTypes
    }
}
