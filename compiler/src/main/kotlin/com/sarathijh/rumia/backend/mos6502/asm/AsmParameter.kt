package com.sarathijh.rumia.backend.mos6502.asm

sealed class AsmParameter(val numberOfBytes: Int)

object ImpliedParameter : AsmParameter(0)

data class ByteParameter(val byte: Int) : AsmParameter(1)

data class WordParameter(val word: Int) : AsmParameter(2)

data class LabelParameter(val label: String, val offset: Int = 0) : AsmParameter(2)

data class RelativeLabelParameter(val label: String) : AsmParameter(1)

data class LabelAddressParameter(val label: String, val byte: Int = 0) : AsmParameter(1)
