package com.sarathijh.rumia.backend.mos6502.asm

sealed class AsmStatement

data class AsmInstruction(val instruction: Instruction, val parameter: AsmParameter) : AsmStatement() {
    init {
        if (instruction.addressingMode == AddressingMode.Relative && parameter is LabelParameter) {
            // Relative addressing mode can have a label parameter.
            // During assembly if the label is too far away, an error will be thrown.
        } else if (parameter.numberOfBytes != instruction.parameterByteCount) {
            throw AsmError(
                    "Given ${parameter.numberOfBytes} parameters, " +
                            "but expected ${instruction.parameterByteCount} for ${instruction.kind.name}" +
                            " instruction with ${instruction.addressingMode.name} address mode")
        }
    }
}

data class AsmLabel(val label: String) : AsmStatement()

data class AsmData(val bytes: List<Int>) : AsmStatement()
