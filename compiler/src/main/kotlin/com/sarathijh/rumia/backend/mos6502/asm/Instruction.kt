package com.sarathijh.rumia.backend.mos6502.asm

import com.sarathijh.rumia.backend.mos6502.asm.AddressingMode.*
import com.sarathijh.rumia.backend.mos6502.asm.InstructionKind.*

data class Instruction(val kind: InstructionKind, val addressingMode: AddressingMode) {
    val opCode by lazy {
        instructionToOpCode[kind]?.get(addressingMode)
                ?: throw AsmError("Instruction '${kind.name}' does not have addressing mode '${addressingMode.name}'")
    }

    val parameterByteCount by lazy { addressingMode.parameterByteCount }
}

class AsmError(override val message: String) : Throwable()

private val instructionToOpCode = mapOf(
        ADC to mapOf(
                Immediate to OpCode(0x69, 2),
                ZeroPage to OpCode(0x65, 3),
                ZeroPageX to OpCode(0x75, 4),
                Absolute to OpCode(0x6D, 4),
                AbsoluteX to OpCode(0x7D, 4),
                AbsoluteY to OpCode(0x79, 4),
                IndirectX to OpCode(0x61, 6),
                IndirectY to OpCode(0x71, 5)),

        AND to mapOf(
                Immediate to OpCode(0x29, 2),
                ZeroPage to OpCode(0x25, 3),
                ZeroPageX to OpCode(0x35, 4),
                Absolute to OpCode(0x2D, 4),
                AbsoluteX to OpCode(0x3D, 4),
                AbsoluteY to OpCode(0x39, 4),
                IndirectX to OpCode(0x21, 6),
                IndirectY to OpCode(0x31, 5)),

        ASL to mapOf(
                Accumulator to OpCode(0x0A, 2),
                ZeroPage to OpCode(0x06, 5),
                ZeroPageX to OpCode(0x16, 6),
                Absolute to OpCode(0x0E, 6),
                AbsoluteX to OpCode(0x1E, 7)),

        BCC to mapOf(Relative to OpCode(0x90, 2)),
        BCS to mapOf(Relative to OpCode(0xB0, 2)),
        BEQ to mapOf(Relative to OpCode(0xF0, 2)),

        BIT to mapOf(
                ZeroPage to OpCode(0x24, 3),
                Absolute to OpCode(0x2C, 4)),

        BMI to mapOf(Relative to OpCode(0x30, 2)),
        BNE to mapOf(Relative to OpCode(0xD0, 2)),
        BPL to mapOf(Relative to OpCode(0x10, 2)),

        BRK to mapOf(Implied to OpCode(0x00, 7)),

        BVC to mapOf(Relative to OpCode(0x50, 2)),
        BVS to mapOf(Relative to OpCode(0x70, 2)),

        CLC to mapOf(Implied to OpCode(0x18, 2)),
        CLD to mapOf(Implied to OpCode(0xD8, 2)),
        CLI to mapOf(Implied to OpCode(0x58, 2)),
        CLV to mapOf(Implied to OpCode(0xB8, 2)),

        CMP to mapOf(
                Immediate to OpCode(0xC9, 2),
                ZeroPage to OpCode(0xC5, 3),
                ZeroPageX to OpCode(0xD5, 4),
                Absolute to OpCode(0xCD, 4),
                AbsoluteX to OpCode(0xDD, 4),
                AbsoluteY to OpCode(0xD9, 4),
                IndirectX to OpCode(0xC1, 6),
                IndirectY to OpCode(0xD1, 5)),

        CPX to mapOf(
                Immediate to OpCode(0xE0, 2),
                ZeroPage to OpCode(0xE4, 3),
                Absolute to OpCode(0xEC, 4)),

        CPY to mapOf(
                Immediate to OpCode(0xC0, 2),
                ZeroPage to OpCode(0xC4, 3),
                Absolute to OpCode(0xCC, 4)),

        DEC to mapOf(
                ZeroPage to OpCode(0xC6, 5),
                ZeroPageX to OpCode(0xD6, 6),
                Absolute to OpCode(0xCE, 6),
                AbsoluteX to OpCode(0xDE, 7)),

        DEX to mapOf(Implied to OpCode(0xCA, 2)),
        DEY to mapOf(Implied to OpCode(0x88, 2)),

        EOR to mapOf(
                Immediate to OpCode(0x49, 2),
                ZeroPage to OpCode(0x45, 3),
                ZeroPageX to OpCode(0x55, 4),
                Absolute to OpCode(0x4D, 4),
                AbsoluteX to OpCode(0x5D, 4),
                AbsoluteY to OpCode(0x59, 4),
                IndirectX to OpCode(0x41, 6),
                IndirectY to OpCode(0x51, 5)),

        INC to mapOf(
                ZeroPage to OpCode(0xE6, 5),
                ZeroPageX to OpCode(0xF6, 6),
                Absolute to OpCode(0xEE, 6),
                AbsoluteX to OpCode(0xFE, 7)),

        INX to mapOf(Implied to OpCode(0xE8, 2)),
        INY to mapOf(Implied to OpCode(0xC8, 2)),

        JMP to mapOf(
                Absolute to OpCode(0x4C, 3),
                Indirect to OpCode(0x6C, 5)),

        JSR to mapOf(Absolute to OpCode(0x20, 6)),

        LDA to mapOf(
                Immediate to OpCode(0xA9, 2),
                ZeroPage to OpCode(0xA5, 3),
                ZeroPageX to OpCode(0xB5, 4),
                Absolute to OpCode(0xAD, 4),
                AbsoluteX to OpCode(0xBD, 4),
                AbsoluteY to OpCode(0xB9, 4),
                IndirectX to OpCode(0xA1, 6),
                IndirectY to OpCode(0xB1, 5)),

        LDX to mapOf(
                Immediate to OpCode(0xA2, 2),
                ZeroPage to OpCode(0xA6, 3),
                ZeroPageY to OpCode(0xB6, 4),
                Absolute to OpCode(0xAE, 4),
                AbsoluteY to OpCode(0xBE, 4)),

        LDY to mapOf(
                Immediate to OpCode(0xA0, 2),
                ZeroPage to OpCode(0xA4, 3),
                ZeroPageX to OpCode(0xB4, 4),
                Absolute to OpCode(0xAC, 4),
                AbsoluteX to OpCode(0xBC, 4)),

        LSR to mapOf(
                Accumulator to OpCode(0x4A, 2),
                ZeroPage to OpCode(0x46, 5),
                ZeroPageX to OpCode(0x56, 6),
                Absolute to OpCode(0x4E, 6),
                AbsoluteX to OpCode(0x5E, 7)),

        NOP to mapOf(Implied to OpCode(0xEA, 2)),

        ORA to mapOf(
                Immediate to OpCode(0x09, 2),
                ZeroPage to OpCode(0x05, 3),
                ZeroPageX to OpCode(0x15, 4),
                Absolute to OpCode(0x0D, 4),
                AbsoluteX to OpCode(0x1D, 4),
                AbsoluteY to OpCode(0x19, 4),
                IndirectX to OpCode(0x01, 6),
                IndirectY to OpCode(0x11, 5)),

        PHA to mapOf(Implied to OpCode(0x48, 3)),
        PHP to mapOf(Implied to OpCode(0x08, 3)),
        PLA to mapOf(Implied to OpCode(0x68, 4)),
        PLP to mapOf(Implied to OpCode(0x28, 4)),

        ROL to mapOf(
                Accumulator to OpCode(0x2A, 2),
                ZeroPage to OpCode(0x26, 5),
                ZeroPageX to OpCode(0x36, 6),
                Absolute to OpCode(0x2E, 6),
                AbsoluteX to OpCode(0x3E, 7)),

        ROR to mapOf(
                Accumulator to OpCode(0x6A, 2),
                ZeroPage to OpCode(0x66, 5),
                ZeroPageX to OpCode(0x76, 6),
                Absolute to OpCode(0x6E, 6),
                AbsoluteX to OpCode(0x7E, 7)),

        RTI to mapOf(Implied to OpCode(0x40, 6)),
        RTS to mapOf(Implied to OpCode(0x60, 6)),

        SBC to mapOf(
                Immediate to OpCode(0xE9, 2),
                ZeroPage to OpCode(0xE5, 3),
                ZeroPageX to OpCode(0xF5, 4),
                Absolute to OpCode(0xED, 4),
                AbsoluteX to OpCode(0xFD, 4),
                AbsoluteY to OpCode(0xF9, 4),
                IndirectX to OpCode(0xE1, 6),
                IndirectY to OpCode(0xF1, 5)),

        SEC to mapOf(Implied to OpCode(0x38, 2)),
        SED to mapOf(Implied to OpCode(0xF8, 2)),
        SEI to mapOf(Implied to OpCode(0x78, 2)),

        STA to mapOf(
                ZeroPage to OpCode(0x85, 3),
                ZeroPageX to OpCode(0x95, 4),
                Absolute to OpCode(0x8D, 4),
                AbsoluteX to OpCode(0x9D, 5),
                AbsoluteY to OpCode(0x99, 5),
                IndirectX to OpCode(0x81, 6),
                IndirectY to OpCode(0x91, 6)),

        STX to mapOf(
                ZeroPage to OpCode(0x86, 3),
                ZeroPageY to OpCode(0x96, 4),
                Absolute to OpCode(0x8E, 4)),

        STY to mapOf(
                ZeroPage to OpCode(0x84, 3),
                ZeroPageX to OpCode(0x94, 4),
                Absolute to OpCode(0x8C, 4)),

        TAX to mapOf(Implied to OpCode(0xAA, 2)),
        TAY to mapOf(Implied to OpCode(0xA8, 2)),
        TSX to mapOf(Implied to OpCode(0xBA, 2)),
        TXA to mapOf(Implied to OpCode(0x8A, 2)),
        TXS to mapOf(Implied to OpCode(0x9A, 2)),
        TYA to mapOf(Implied to OpCode(0x98, 2)),

        SYS to mapOf(Immediate to OpCode(0x80, 2))
)

data class OpCode(val code: Int, val cycles: Int)

enum class InstructionKind {
    ADC, BMI, CLD, DEX, JSR, PHA, RTS, STY,
    AND, BNE, CLI, DEY, LDA, PHP, SBC, TAX,
    ASL, BPL, CLV, EOR, LDX, PLA, SEC, TAY,
    BCC, BRK, CMP, INC, LDY, PLP, SED, TSX,
    BCS, BVC, CPX, INX, LSR, ROL, SEI, TXA,
    BEQ, BVS, CPY, INY, NOP, ROR, STA, TXS,
    BIT, CLC, DEC, JMP, ORA, RTI, STX, TYA,
    SYS,
}

enum class AddressingMode(val parameterByteCount: Int) {
    Immediate(1),
    Accumulator(0),
    ZeroPage(1),
    ZeroPageX(1),
    ZeroPageY(1),
    Absolute(2),
    AbsoluteX(2),
    AbsoluteY(2),
    Indirect(2),
    IndirectX(1),
    IndirectY(1),
    Relative(1),
    Implied(0),
}
