package com.sarathijh.rumia.util

fun bigramComparison(lhs: String, rhs: String): Float {
    val leftBigrams = lhs.bigrams()
    val rightBigrams = rhs.bigrams()

    return (2f * leftBigrams.intersect(rightBigrams).size) / (leftBigrams.size + rightBigrams.size)
}

fun String.bigrams() = this.windowed(2)
