package com.sarathijh.rumia.util

fun <T> List<T>.forEachBetween(between: () -> Unit, action: (T) -> Unit) {
    forEachIndexed { index, item ->
        if (index > 0) {
            between()
        }
        action(item)
    }
}
