package com.sarathijh.rumia

import com.sarathijh.rumia.frontend.lex.SourceRange

open class CompilationError(override val message: String, val sourceRange: SourceRange) : Throwable()

class UnimplementedError(message: String, sourceRange: SourceRange) : CompilationError("Unimplemented: $message", sourceRange)
