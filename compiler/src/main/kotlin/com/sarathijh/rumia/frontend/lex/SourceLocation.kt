package com.sarathijh.rumia.frontend.lex

data class SourceLocation(val line: Int, val column: Int, val isAfter: Boolean = false) {
    val after by lazy { SourceLocation(line, column, true) }
}

data class SourceRange(val start: SourceLocation, val endInclusive: SourceLocation) {
    val after by lazy { SourceRange(endInclusive.after, endInclusive.after) }
}
