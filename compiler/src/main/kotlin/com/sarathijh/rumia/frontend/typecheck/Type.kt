package com.sarathijh.rumia.frontend.typecheck

sealed class Type(open val bytes: Int) {

    open val memberTypes = mapOf<String, Type>()

    open fun isSubtypeOf(otherType: Type) = (otherType == this)
}

fun Type.isNotSubtypeOf(otherType: Type) = !isSubtypeOf(otherType)

data class ReferenceType(val innerType: Type) : Type(innerType.bytes) { override fun toString() = innerType.toString() }

object Untyped : Type(0) { override fun toString() = "<Untyped>" }
object VoidType : Type(0) { override fun toString() = "Void" }

object IntType : Type(1) {
    override fun toString() = "Int"

    override fun isSubtypeOf(otherType: Type) = when (otherType) {
        is Int16Type -> true
        else -> super.isSubtypeOf(otherType)
    }
}

object Int16Type : Type(2) { override fun toString() = "Int16" }

object UIntType : Type(1) {
    override fun toString() = "UInt"

    override fun isSubtypeOf(otherType: Type) = when (otherType) {
        is IntType -> true
        is Int16Type -> true
        is UInt16Type -> true
        else -> super.isSubtypeOf(otherType)
    }
}

object UInt16Type : Type(2) {
    override fun toString() = "UInt16"

    override fun isSubtypeOf(otherType: Type) = when (otherType) {
        is Int16Type -> true
        else -> super.isSubtypeOf(otherType)
    }
}

object BoolType : Type(1) { override fun toString() = "Bool" }

object StringType : Type(2) {
    override fun toString() = "String"

    override val memberTypes = mapOf(
            "length" to IntType)
}

open class ClassType(val name: String) : Type(0) {
    override fun toString() = name

    override val bytes get() = memberTypes.values.sumBy { it.bytes }

    override val memberTypes = mutableMapOf<String, Type>()
    val instanceMemberTypes = mutableMapOf<String, Type>()
}

class InstanceType(val classType: ClassType) : Type(2) {
    override fun toString() = classType.name

    override val bytes get() = memberTypes.values.sumBy { it.bytes }

    override val memberTypes = classType.instanceMemberTypes
}

object IntClassType : ClassType(IntType.toString()) {

    override val memberTypes = mutableMapOf<String, Type>(
            "min" to IntType,
            "max" to IntType)
}

object Int16ClassType : ClassType(IntType.toString()) {

    override val memberTypes = mutableMapOf<String, Type>(
            "min" to Int16Type,
            "max" to Int16Type)
}

data class ArrayType(val elementType: Type, val size: Int) : Type(elementType.bytes * size) {
    override fun toString() = "$elementType[$size]"

    override val memberTypes = mapOf(
            "length" to IntType,
            "sum" to FunctionType(IntType, emptyList(), true))
}

data class FunctionType(val returnType: Type, val parameterTypes: List<Type>, val isNative: Boolean = false) : Type(2) {
    override fun toString() = "(${parameterTypes.joinToString(", ")}) -> $returnType"
}

data class FunctionSetType(val functionTypes: MutableSet<FunctionType>) : Type(2) {
    override fun toString() = "(${functionTypes.joinToString(", ")})"

    fun addFunctionType(functionType: FunctionType) {
        functionTypes.add(functionType)
    }

    fun getFunctionTypeForSignature(argumentTypes: List<Type>): FunctionType? {
        return try {
            functionTypes.first { argumentTypes.isSubtypeOf(it.parameterTypes) }
        } catch (ex: NoSuchElementException) {
            null
        }
    }
}

fun List<Type>.isSubtypeOf(other: List<Type>): Boolean {
    return if (size == other.size) {
        zip(other).all { (item, otherItem) -> item.isSubtypeOf(otherItem) }
    } else {
        false
    }
}
