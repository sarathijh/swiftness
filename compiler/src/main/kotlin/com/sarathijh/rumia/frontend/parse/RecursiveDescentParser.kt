package com.sarathijh.rumia.frontend.parse

import com.sarathijh.rumia.frontend.ast.AstNode
import com.sarathijh.rumia.frontend.ast.AstNodeList
import com.sarathijh.rumia.frontend.lex.*

class ParseError(override val message: String, val sourceRange: SourceRange) : Throwable()

open class RecursiveDescentParser(protected val lexer: LexIterator) {

    protected fun lexMatch(kind: String): Token? {
        val token = lexer.peek()
        if (token.kind == kind) {
            lexer.next()
            return token
        }
        return null
    }

    protected fun <T> expectIf(kind: String, parser: () -> T?, error: String): T? {
        val markerToken = lexMatch(kind)
                ?: return null

        lexMatch(Lexer.TkNewline)

        return parser()
                ?: throw ParseError(
                        error,
                        markerToken.sourceRange.after)
    }

    protected fun <T> oneOf(vararg functions: () -> T?) = functions.firstNotNull { it() }

    protected fun oneOf(vararg kinds: String) = kinds.firstNotNull(::lexMatch)

    private fun <T, U> Array<T>.firstNotNull(transform: (T) -> U?): U? {
        for (element in this) {
            return transform(element) ?: continue
        }
        return null
    }

    protected fun <T : AstNode> parseSurrounded(name: String, expectBefore: String, expectAfter: String, parser: () -> T?): Pair<T, SourceRange>? {
        val beforeToken = lexMatch(expectBefore)
                ?: return null

        lexMatch(Lexer.TkNewline)

        val result = parser()

        lexMatch(Lexer.TkNewline)

        val afterToken = lexMatch(expectAfter)
                ?: throw ParseError(
                        "Expected '$expectAfter' at end of $name",
                        (result?.sourceRange ?: beforeToken.sourceRange).after)

        return if (result != null) {
            result to SourceRange(beforeToken.sourceRange.start, afterToken.sourceRange.endInclusive)
        } else {
            null
        }
    }

    protected inline fun <reified T : AstNode> parseListOf(parser: () -> T?, expectAfter: String? = null): AstNodeList<T> {
        val list = mutableListOf<T>()

        while (true) {
            lexMatch(Lexer.TkNewline)
            list.add(parser() ?: break)
        }

        if (expectAfter != null) {
            lexMatch(Lexer.TkNewline)
            val token = lexer.peek()
            if (token.kind != expectAfter) {
                throw ParseError("Unexpected '${token.value}'", token.sourceRange)
            }
        }

        return AstNodeList(list)
    }

    protected inline fun <reified T : AstNode> parseDelimitedListOf(parser: () -> T?, delimiter: String, itemPrefix: String? = null): AstNodeList<T> {
        val list = mutableListOf<T>()

        val firstItem = parser()
        if (firstItem != null) {

            list.add(firstItem)

            while (true) {

                lexMatch(Lexer.TkNewline)

                val delimiterToken = lexMatch(delimiter)
                        ?: break

                lexMatch(Lexer.TkNewline)

                list.add(parser()
                        ?: throw ParseError(
                                "Unexpected '$delimiter' separator",
                                delimiterToken.sourceRange))
            }

            if (itemPrefix != null && lexer.peek().kind == itemPrefix) {
                throw ParseError("Expected '$delimiter' separator", list.last().sourceRange.after)
            }
        }

        return AstNodeList(list)
    }
}
