package com.sarathijh.rumia.frontend.typecheck

open class TypeEnvironment(private val parent: TypeEnvironment? = null) {

    private val typeForName = mutableMapOf<String, Type>()

    val allDeclaredNames: Set<String> by lazy { typeForName.keys + parent?.allDeclaredNames }

    open val functionReturnType: Type? by lazy { parent?.functionReturnType }

    operator fun contains(nameAndType: Pair<String, Type>): Boolean {
        val (name, type) = nameAndType
        return when (type) {
            is FunctionType -> {
                val existingDeclaration = typeForName[name]
                when (existingDeclaration) {
                    is FunctionSetType -> (type !in existingDeclaration.functionTypes)
                    is FunctionType -> (type != existingDeclaration)
                    else -> false
                }
            }
            else -> when {
                name in typeForName -> true
                parent != null -> parent.contains(nameAndType)
                else -> false
            }
        }
    }

    operator fun get(name: String): Type? = typeForName[name] ?: parent?.get(name)

    operator fun set(name: String, type: Type) {
        when {
            type is FunctionType -> declareFunctionType(name, type)
            name !in typeForName -> typeForName[name] = type

            else -> throw Exception("'$name' previously declared with type '${typeForName[name]}', redeclared with type '$type'")
        }
    }

    private fun declareFunctionType(name: String, type: FunctionType) {
        val existingDeclaration = typeForName[name]
        when (existingDeclaration) {
            is FunctionSetType -> {
                if (type !in existingDeclaration.functionTypes) {
                    existingDeclaration.addFunctionType(type)
                }
            }

            is FunctionType -> {
                if (type != existingDeclaration) {
                    typeForName[name] = FunctionSetType(mutableSetOf(existingDeclaration, type))
                }
            }

            null -> typeForName[name] = type

            else -> throw Exception("'$name' previously declared with type '${typeForName[name]}', redeclared with type '$type'")
        }
    }

    private operator fun <T> Set<T>.plus(other: Set<T>?): Set<T> = if (other == null) this else this.union(other)
}

class FunctionTypeEnvironment(returnType: Type, parent: TypeEnvironment) : TypeEnvironment(parent) {
    override val functionReturnType = returnType
}
