package com.sarathijh.rumia.frontend.lex

import java.io.FileReader
import java.util.*

class Lexer(scanner: Scanner) : LexIterator(scanner) {

    constructor(filePath: String) : this(Scanner(FileReader(filePath)))

    init {
        addTokenPattern("\\b${keywords.regexConditional}\\b")
        addTokenPattern(boolLiteralPattern, TkBoolLiteral)
        addTokenPattern("\\b${typeNames.regexConditional}\\b", TkTypeName)
        addTokenPattern(punctuation.regexConditional)
        addTokenPattern(identifierPattern, TkIdentifier)
        addTokenPattern(integerLiteralPattern, TkIntLiteral)
        addTokenPattern("\n+", TkNewline)

        addTokenPattern("\"((?<=\\\\)\"|[^\"])*\"", TkStringLiteral) {
            it.substring(1, it.length - 1)
                    .replace("\\n", "\n")
                    .replace("\\\"", "\"")
        }

        addIgnorePattern("[ \t]+")
        addIgnorePattern("//.*?(?=\n)")
        addIgnoreRange("/\\*", "\\*/")
    }

    companion object {
        private val List<String>.regexConditional get() = "(${joinToString("|") { Regex.escape(it) }})"

        const val TkIdentifier = "Identifier"
        const val TkIntLiteral = "IntLiteral"
        const val TkBoolLiteral = "BoolLiteral"
        const val TkStringLiteral = "StringLiteral"
        const val TkTypeName = "TypeName"
        const val TkNewline = "Newline"

        private const val fullWidthRoman = "\\uFF00-\\uFFEF"
        private const val japanese = "\\u3000-\\u30FF"
        private const val chinese = "\\u2E80-\\u2FD5\\u3400-\\u4DBF\\u4E00-\\u9FCC"

        private const val identifierStart = "a-zA-Z_$fullWidthRoman$japanese$chinese"
        private const val identifierRest = "0-9$identifierStart"

        const val identifierPattern = "\\b[$identifierStart][$identifierRest]*\\b"

        const val integerLiteralPattern = "0b[0-1]+|0x[0-9a-fA-F]+|[0-9]+"

        const val boolLiteralPattern = "\\b(true|false)\\b"

        val keywords = listOf("var", "if", "else", "while", "for", "in", "fun", "return")

        val typeNames = listOf("Int16", "Int", "UInt16", "UInt", "Bool", "String")

        val punctuation = listOf(
                "!=", "!", "~", "==", "=", "<=", "<<", "<", ">=", ">>", ">", "%=", "%", "*=", "*", "+=", "+", "->",
                "-=", "-", "&&", "&=", "&", "||", "|=", "|", "^=", "^", ":", ",", ".", "{", "}", "(", ")", "[", "]")
    }
}
