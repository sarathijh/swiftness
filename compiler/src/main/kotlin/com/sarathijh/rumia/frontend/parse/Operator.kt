package com.sarathijh.rumia.frontend.parse

internal enum class Operator(val symbol: String, val precedence: Int, val isPrefix: Boolean, val isRightAssociative: Boolean) {

    MemberAccess  (".", precedence = 200, isPrefix = false, isRightAssociative = false),

    LogicalNot    ("prefix!", precedence = 180, isPrefix = true, isRightAssociative = false),
    BitwiseNot    ("prefix~", precedence = 180, isPrefix = true, isRightAssociative = false),

    Negation      ("prefix-", precedence = 170, isPrefix = true, isRightAssociative = false),
    Positive      ("prefix+", precedence = 170, isPrefix = true, isRightAssociative = false),

    LeftShift     ("prefix<<", precedence = 160, isPrefix = true, isRightAssociative = false),
    RightShift    ("prefix>>", precedence = 160, isPrefix = true, isRightAssociative = false),

    Product       ("*", precedence = 150, isPrefix = false, isRightAssociative = false),
    Modulus       ("%", precedence = 150, isPrefix = false, isRightAssociative = false),
    BitwiseAnd    ("&", precedence = 150, isPrefix = false, isRightAssociative = false),

    Addition      ("+", precedence = 140, isPrefix = false, isRightAssociative = false),
    Subtraction   ("-", precedence = 140, isPrefix = false, isRightAssociative = false),
    BitwiseOr     ("|", precedence = 140, isPrefix = false, isRightAssociative = false),
    BitwiseXor    ("^", precedence = 140, isPrefix = false, isRightAssociative = false),

    Greater       (">", precedence = 130, isPrefix = false, isRightAssociative = false),
    GreaterOrEqual(">=", precedence = 130, isPrefix = false, isRightAssociative = false),
    Less          ("<", precedence = 130, isPrefix = false, isRightAssociative = false),
    LessOrEqual   ("<=", precedence = 130, isPrefix = false, isRightAssociative = false),
    Equal         ("==", precedence = 130, isPrefix = false, isRightAssociative = false),
    NotEqual      ("!=", precedence = 130, isPrefix = false, isRightAssociative = false),

    LogicalAnd    ("&&", precedence = 120, isPrefix = false, isRightAssociative = false),

    LogicalOr     ("||", precedence = 110, isPrefix = false, isRightAssociative = false);

    companion object {

        fun fromSymbol(symbol: String) = symbolToOperator[symbol]

        private val symbolToOperator by lazy { values().map { it.symbol to it }.toMap() }
    }
}
