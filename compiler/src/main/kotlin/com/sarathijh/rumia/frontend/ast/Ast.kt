package com.sarathijh.rumia.frontend.ast

import com.sarathijh.rumia.frontend.lex.SourceRange
import com.sarathijh.rumia.backend.mos6502.codegen.CodeGenEnvironment
import com.sarathijh.rumia.frontend.lex.SourceLocation
import com.sarathijh.rumia.frontend.typecheck.FunctionType
import com.sarathijh.rumia.frontend.typecheck.Type
import com.sarathijh.rumia.frontend.typecheck.Untyped

sealed class AstNode {
    abstract val sourceRange: SourceRange
    var type: Type = Untyped
}

data class AstNodeList<T : AstNode>(val list: List<T>) : AstNode(), List<T> by list {
    override val sourceRange = if (isNotEmpty()) {
        SourceRange(first().sourceRange.start, last().sourceRange.endInclusive)
    } else {
        SourceRange(SourceLocation(0, 0), SourceLocation(0, 0))
    }
}

sealed class Statement : AstNode() {
    var nextLabel: String = "<NoLabel>"
    var returnType: Type? = null
}

data class BlockStatement(val statements: AstNodeList<Statement>, override val sourceRange: SourceRange) : Statement() {
    var codeGenEnvironment: CodeGenEnvironment? = null
}

data class ExpressionStatement(val expression: Expression) : Statement() {
    override val sourceRange = expression.sourceRange
}

data class AssignStatement(val identifierExpression: IdentifierExpression, val valueExpression: Expression) : Statement() {
    override val sourceRange = SourceRange(identifierExpression.sourceRange.start, valueExpression.sourceRange.endInclusive)
}

data class SubscriptAssignStatement(val subscriptExpression: SubscriptExpression, val valueExpression: Expression) : Statement() {
    override val sourceRange = SourceRange(subscriptExpression.sourceRange.start, valueExpression.sourceRange.endInclusive)
}

data class IfStatement(
        val conditionExpression: Expression,
        val bodyStatement: BlockStatement,
        val elseStatement: Statement?,
        override val sourceRange: SourceRange)
    : Statement()

data class WhileStatement(
        val conditionExpression: Expression,
        val bodyStatement: BlockStatement,
        override val sourceRange: SourceRange)
    : Statement()

data class ForInStatement(
        val identifierExpression: IdentifierExpression,
        val arrayExpression: Expression,
        val bodyStatement: BlockStatement,
        override val sourceRange: SourceRange)
    : Statement()

data class ReturnStatement(
        val valueExpression: Expression?,
        override val sourceRange: SourceRange)
    : Statement()

sealed class Declaration : Statement()

data class VariableDeclaration(
        val identifierExpression: IdentifierExpression,
        val typeAnnotation: TypeAnnotation?,
        val valueExpression: Expression?,
        override val sourceRange: SourceRange)
    : Declaration()

data class FunctionDeclaration(
        val identifierExpression: IdentifierExpression,
        val returnTypeAnnotation: TypeAnnotation?,
        val parameters: AstNodeList<FunctionParameter>,
        val bodyStatement: BlockStatement,
        val inferReturnType: Boolean,
        override val sourceRange: SourceRange)
    : Declaration()

data class FunctionParameter(val identifierExpression: IdentifierExpression, val typeAnnotation: TypeAnnotation) : AstNode() {
    override val sourceRange = SourceRange(identifierExpression.sourceRange.start, typeAnnotation.sourceRange.endInclusive)
}

sealed class Expression : AstNode() {
    var trueLabel: String = "<NoLabel>"
    var falseLabel: String = "<NoLabel>"
}

data class IdentifierExpression(val name: String, override val sourceRange: SourceRange) : Expression()

data class IntLiteralExpression(val intValue: Int, override val sourceRange: SourceRange) : Expression()

data class BoolLiteralExpression(val boolValue: Boolean, override val sourceRange: SourceRange) : Expression()

data class StringLiteralExpression(val stringValue: String, override val sourceRange: SourceRange) : Expression()

data class ArrayLiteralExpression(val elementExpressions: AstNodeList<Expression>, override val sourceRange: SourceRange) : Expression()

data class FunctionCallExpression(
        val identifierExpression: IdentifierExpression,
        val arguments: AstNodeList<Expression>,
        override val sourceRange: SourceRange)
    : Expression()
{
    lateinit var functionType: FunctionType
}

data class MemberAccessExpression(val objectExpression: Expression, val memberExpression: IdentifierExpression) : Expression() {
    override val sourceRange = SourceRange(objectExpression.sourceRange.start, memberExpression.sourceRange.endInclusive)
}

data class SubscriptExpression(
        val identifierExpression: IdentifierExpression,
        val indexExpression: Expression,
        override val sourceRange: SourceRange)
    : Expression()

data class InfixExpression(
        val operator: String,
        val leftExpression: Expression,
        val rightExpression: Expression,
        val operatorSourceRange: SourceRange)
    : Expression()
{
    override val sourceRange = SourceRange(leftExpression.sourceRange.start, rightExpression.sourceRange.endInclusive)
}

data class PrefixExpression(
        val operator: String,
        val subExpression: Expression,
        val operatorSourceRange: SourceRange)
    : Expression()
{
    override val sourceRange = SourceRange(operatorSourceRange.start, subExpression.sourceRange.endInclusive)
}

data class TypeNameExpression(val typeAnnotation: TypeAnnotation) : Expression() {
    override val sourceRange = typeAnnotation.sourceRange
}

sealed class TypeAnnotation : AstNode()

data class IntTypeAnnotation(override val sourceRange: SourceRange) : TypeAnnotation()

data class Int16TypeAnnotation(override val sourceRange: SourceRange) : TypeAnnotation()

data class UIntTypeAnnotation(override val sourceRange: SourceRange) : TypeAnnotation()

data class UInt16TypeAnnotation(override val sourceRange: SourceRange) : TypeAnnotation()

data class BoolTypeAnnotation(override val sourceRange: SourceRange) : TypeAnnotation()

data class StringTypeAnnotation(override val sourceRange: SourceRange) : TypeAnnotation()

data class ClassTypeAnnotation(val className: String, override val sourceRange: SourceRange) : TypeAnnotation()

data class ArrayTypeAnnotation(
        val elementTypeAnnotation: TypeAnnotation,
        val sizeExpression: IntLiteralExpression,
        override val sourceRange: SourceRange)
    : TypeAnnotation()
