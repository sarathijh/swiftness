package com.sarathijh.rumia.frontend.typecheck

import com.sarathijh.rumia.UnimplementedError
import com.sarathijh.rumia.frontend.ast.*
import com.sarathijh.rumia.frontend.lex.SourceRange
import com.sarathijh.rumia.frontend.parse.ParseError

class TypeChecker(private var typeEnvironment: TypeEnvironment = TypeEnvironment()) {

    fun typeCheck(node: AstNode): TypeEnvironment {
        checkFunctionSignatures(node)
        check(node)
        return typeEnvironment
    }

    private fun checkFunctionSignatures(node: AstNode) {
        when (node) {
            is BlockStatement -> node.statements.forEach { checkFunctionSignatures(it) }

            is FunctionDeclaration -> {
                val returnAnnotationType = if (node.returnTypeAnnotation != null) {
                    check(node.returnTypeAnnotation)
                } else {
                    VoidType
                }
                val parameterTypes = node.parameters.map { argument -> check(argument.typeAnnotation) }

                node.type = FunctionType(returnAnnotationType, parameterTypes)

                if ((node.identifierExpression.name to node.type) in typeEnvironment) {
                    // TODO: Get conflicting function declaration from function set
                    throw InvalidRedeclarationError(node.identifierExpression.name, node.identifierExpression.sourceRange)
                }

                typeEnvironment[node.identifierExpression.name] = node.type
            }
        }
    }

    private fun check(node: AstNode): Type {
        node.type = when (node) {
            is BlockStatement -> {
                node.statements.forEach {
                    check(it)
                    node.returnType = node.returnType ?: it.returnType
                }
                VoidType
            }

            is AstNodeList<*> -> VoidType

            is ExpressionStatement -> check(node.expression)

            is AssignStatement -> {
                val variableType = check(node.identifierExpression)
                val valueType = check(node.valueExpression)

                if (!valueType.isSubtypeOf(variableType)) {
                    throw TypeError(
                            "Cannot assign value of type '$valueType' to type '$variableType'",
                            node.valueExpression.sourceRange)
                }

                VoidType
            }

            is SubscriptAssignStatement -> {
                val variableType = check(node.subscriptExpression)
                val valueType = check(node.valueExpression)

                if (!valueType.isSubtypeOf(variableType)) {
                    throw TypeError(
                            "Cannot assign value of type '$valueType' to element type '$variableType'",
                            node.valueExpression.sourceRange)
                }

                VoidType
            }

            is IfStatement -> {
                val conditionType = check(node.conditionExpression)
                if (conditionType.isNotSubtypeOf(BoolType)) {
                    throw TypeMismatchError(BoolType, conditionType, node.conditionExpression.sourceRange)
                }

                inChildEnvironment {
                    check(node.bodyStatement)
                }

                if (node.elseStatement != null) {
                    inChildEnvironment {
                        check(node.elseStatement)
                    }

                    if (node.bodyStatement.returnType != null && node.elseStatement.returnType != null) {
                        // TODO: Compare return types here?
                        node.returnType = node.bodyStatement.returnType
                    }
                }

                VoidType
            }

            is WhileStatement -> {
                val conditionType = check(node.conditionExpression)
                if (conditionType.isNotSubtypeOf(BoolType)) {
                    throw TypeMismatchError(BoolType, conditionType, node.conditionExpression.sourceRange)
                }

                inChildEnvironment {
                    check(node.bodyStatement)
                }

                VoidType
            }

            is ForInStatement -> {
                val arrayType = check(node.arrayExpression)
                if (arrayType !is ArrayType) {
                    throw TypeError(
                            "Cannot iterate over value of type '$arrayType'",
                            node.arrayExpression.sourceRange)
                }

                inChildEnvironment {
                    if ((node.identifierExpression.name to arrayType.elementType) in typeEnvironment) {
                        throw InvalidRedeclarationError(node.identifierExpression.name, node.identifierExpression.sourceRange)
                    }

                    typeEnvironment[node.identifierExpression.name] = arrayType.elementType

                    node.identifierExpression.type = arrayType.elementType

                    check(node.bodyStatement)
                }

                if (arrayType.size > 0) {
                    node.returnType = node.bodyStatement.returnType
                }

                VoidType
            }

            is ReturnStatement -> {
                val functionReturnType = typeEnvironment.functionReturnType
                        ?: throw TypeError(
                                "Return invalid outside of a function",
                                node.sourceRange)

                if (node.valueExpression != null) {
                    val returnType = check(node.valueExpression)
                    if (!returnType.isSubtypeOf(functionReturnType)) {
                        if (functionReturnType == VoidType) {
                            throw TypeError(
                                    "Unexpected non-void return value in void function",
                                    node.valueExpression.sourceRange)
                        } else {
                            throw TypeError(
                                    "Cannot convert return expression of type '$returnType' to return type '$functionReturnType'",
                                    node.valueExpression.sourceRange)
                        }
                    }
                    node.returnType = returnType
                    returnType
                } else {
                    if (functionReturnType != VoidType) {
                        throw TypeError(
                                "Non-void function should return a value",
                                node.sourceRange.after)
                    }
                    node.returnType = VoidType
                    VoidType
                }
            }

            is VariableDeclaration -> {
                var annotatedType = if (node.typeAnnotation != null) check(node.typeAnnotation) else null
                val valueType = if (node.valueExpression != null) check(node.valueExpression) else null

                if (annotatedType == null) {
                    if (valueType == VoidType) {
                        throw TypeError(
                                "Cannot assign value of type '$VoidType'",
                                node.valueExpression!!.sourceRange)
                    }
                    annotatedType = valueType
                } else if (valueType != null && !valueType.isSubtypeOf(annotatedType)) {
                    throw TypeMismatchError(annotatedType, valueType, node.valueExpression!!.sourceRange)
                }

                if ((node.identifierExpression.name to annotatedType!!) in typeEnvironment) {
                    throw InvalidRedeclarationError(node.identifierExpression.name, node.identifierExpression.sourceRange)
                }

                typeEnvironment[node.identifierExpression.name] = annotatedType

                annotatedType
            }

            is FunctionDeclaration -> {
                val functionType = node.type as FunctionType
                inFunctionEnvironment (functionType.returnType) {
                    node.parameters.forEach { check(it) }

                    check(node.bodyStatement)
                }

                node.returnType = node.bodyStatement.returnType
                if (node.bodyStatement.returnType == null && functionType.returnType != VoidType) {
                    throw TypeError(
                            "Missing return in a function expected to return '${functionType.returnType}'",
                            SourceRange(node.bodyStatement.sourceRange.endInclusive, node.bodyStatement.sourceRange.endInclusive))
                }

                VoidType
            }

            is FunctionParameter -> {
                val type = check(node.typeAnnotation)

                if ((node.identifierExpression.name to type) in typeEnvironment) {
                    throw InvalidRedeclarationError(node.identifierExpression.name, node.identifierExpression.sourceRange)
                }

                typeEnvironment[node.identifierExpression.name] = type

                type
            }

            is IdentifierExpression -> {
                typeEnvironment[node.name]
                        ?: throw UnresolvedIdentifierError(node.name, typeEnvironment, node.sourceRange)
            }

            is TypeNameExpression -> when (node.typeAnnotation) {
                is IntTypeAnnotation -> IntClassType
                is Int16TypeAnnotation -> Int16ClassType
                else -> throw TypeError("Cannot use type name '${node.typeAnnotation}' as an object", node.sourceRange)
            }

            is IntLiteralExpression -> {
                if (node.intValue > 255) {
                    Int16Type
                } else {
                    IntType
                }
            }
            is BoolLiteralExpression -> BoolType
            is StringLiteralExpression -> StringType
            is ArrayLiteralExpression -> {
                if (node.elementExpressions.isEmpty()) {
                    throw TypeError("Array literal must have at least one element", node.sourceRange)
                } else if (node.elementExpressions.size > 255) {
                    throw UnimplementedError("max array size is currently 255", node.sourceRange)
                }

                var elementType: Type = Untyped
                node.elementExpressions.forEach {
                    val type = check(it)
                    if (elementType == Untyped) {
                        elementType = type
                    } else if (!type.isSubtypeOf(elementType)) {
                        throw TypeError("Array elements must all be of the same type", it.sourceRange)
                    }
                }

                ArrayType(elementType, node.elementExpressions.size)
            }

            is FunctionCallExpression -> {
                val functionType = check(node.identifierExpression)
                val argumentTypes = node.arguments.map { check(it) }

                when (functionType) {
                    is FunctionSetType -> {
                        val overloadType = functionType.getFunctionTypeForSignature(argumentTypes)

                        val returnType = overloadType?.returnType
                                ?: throw ArgumentListMismatchError(
                                        node.identifierExpression.name,
                                        argumentTypes,
                                        functionType,
                                        node.identifierExpression.sourceRange)

                        node.functionType = overloadType

                        returnType
                    }

                    is FunctionType -> {
                        when {
                            node.arguments.size < functionType.parameterTypes.size -> {
                                throw TypeError(
                                        "Missing argument for parameter #${node.arguments.size + 1} in call",
                                        if (node.arguments.isEmpty()) {
                                            SourceRange(
                                                    node.arguments.sourceRange.start.after,
                                                    node.arguments.sourceRange.start.after)
                                        } else {
                                            node.arguments.last().sourceRange.after
                                        })
                            }
                            node.arguments.size > functionType.parameterTypes.size -> {
                                throw TypeError(
                                        "Extra argument in call",
                                        node.arguments[functionType.parameterTypes.size].sourceRange)
                            }
                            else -> {
                                node.arguments.zip(argumentTypes).zip(functionType.parameterTypes).forEach { (argument, parameterType) ->
                                    val (argumentExpression, argumentType) = argument

                                    if (!argumentType.isSubtypeOf(parameterType)) {
                                        throw TypeError(
                                                "Cannot convert value of type '$argumentType' to expected argument type '$parameterType'",
                                                argumentExpression.sourceRange)
                                    }
                                }

                                node.functionType = functionType

                                functionType.returnType
                            }
                        }
                    }

                    else -> {
                        throw TypeError(
                                "Cannot call value of non-function type '$functionType'",
                                node.identifierExpression.sourceRange)
                    }
                }
            }

            is MemberAccessExpression -> {
                val objectType = check(node.objectExpression)
                val memberName = node.memberExpression.name

                val memberType = objectType.memberTypes[memberName]

                if (memberType == null) {
                    val classType = classTypeFromInstanceType(objectType)

                    throw ParseError(
                            if (classType?.memberTypes?.containsKey(memberName) == true) {
                                "Static member '$memberName' cannot be used on instance of type '$classType'"
                            } else {
                                "Value of type '$objectType' has no member '$memberName'"
                            },
                            node.memberExpression.sourceRange)
                }

                memberType
            }

            is SubscriptExpression -> {
                val arrayType = check(node.identifierExpression)

                if (arrayType !is ArrayType) {
                    throw TypeError(
                            "Cannot subscript a value of type '$arrayType'",
                            node.identifierExpression.sourceRange)
                }

                val subscriptType = check(node.indexExpression)

                if (subscriptType != IntType) {
                    throw TypeError(
                            "Cannot subscript a value of type '$arrayType' with an index of type '$subscriptType'",
                            node.indexExpression.sourceRange)
                }

                arrayType.elementType
            }

            is InfixExpression -> {
                val operatorType = typeEnvironment["infix${node.operator}"]
                val argumentTypes = listOf(check(node.leftExpression), check(node.rightExpression))

                val functionType = when (operatorType) {
                    is FunctionSetType -> operatorType.getFunctionTypeForSignature(argumentTypes)
                    is FunctionType -> {
                        if (argumentTypes.isSubtypeOf(operatorType.parameterTypes)) {
                            operatorType
                        } else {
                            null
                        }
                    }
                    else -> {
                        throw TypeError(
                                "'${node.operator}' is not an infix operator",
                                node.operatorSourceRange)
                    }
                }
                        ?: throw TypeError(
                                "Infix operator '${node.operator}' cannot be applied to operands of type '${argumentTypes[0]}' and '${argumentTypes[1]}'",
                                node.operatorSourceRange)

                functionType.returnType
            }

            is PrefixExpression -> {
                val operatorType = typeEnvironment["prefix${node.operator}"]
                val argumentTypes = listOf(check(node.subExpression))

                val functionType = when (operatorType) {
                    is FunctionSetType -> operatorType.getFunctionTypeForSignature(argumentTypes)
                    is FunctionType -> {
                        if (argumentTypes.isSubtypeOf(operatorType.parameterTypes)) {
                            operatorType
                        } else {
                            null
                        }
                    }
                    else -> {
                        throw TypeError(
                                "'${node.operator}' is not a prefix operator",
                                node.operatorSourceRange)
                    }
                }
                        ?: throw TypeError(
                                "Prefix operator '${node.operator}' cannot be applied to operand '${argumentTypes[0]}'",
                                node.operatorSourceRange)

                functionType.returnType
            }

            is IntTypeAnnotation -> IntType
            is Int16TypeAnnotation -> Int16Type
            is UIntTypeAnnotation -> UIntType
            is UInt16TypeAnnotation -> UInt16Type
            is BoolTypeAnnotation -> BoolType
            is StringTypeAnnotation -> StringType
            is ClassTypeAnnotation -> typeEnvironment[node.className] as? ClassType
                    ?: throw TypeError(
                            "Use of undeclared type '${node.className}'",
                            node.sourceRange)

            is ArrayTypeAnnotation -> {
                if (node.sizeExpression.intValue < 1) {
                    throw TypeError(
                            "Array type must have a positive size",
                            node.sizeExpression.sourceRange)
                } else if (node.sizeExpression.intValue > 255) {
                    throw UnimplementedError(
                            "max array size is currently 255",
                            node.sizeExpression.sourceRange)
                }

                ArrayType(check(node.elementTypeAnnotation), node.sizeExpression.intValue)
            }
        }

        return node.type
    }

    private fun classTypeFromInstanceType(instanceType: Type): ClassType? {
        return when (instanceType) {
            is IntType -> IntClassType
            is Int16Type -> Int16ClassType
            is InstanceType -> instanceType.classType
            else -> null
        }
    }

    private fun inChildEnvironment(function: () -> Unit) {
        inEnvironment(TypeEnvironment(parent = typeEnvironment), function)
    }

    private fun inFunctionEnvironment(returnType: Type, function: () -> Unit) {
        val functionEnvironment = FunctionTypeEnvironment(returnType, parent = typeEnvironment)
        inEnvironment(functionEnvironment, function)
    }

    private fun inEnvironment(newTypeEnvironment: TypeEnvironment, function: () -> Unit) {
        val oldEnvironment = typeEnvironment
        typeEnvironment = newTypeEnvironment
        function()
        typeEnvironment = oldEnvironment
    }
}
