package com.sarathijh.rumia.frontend.parse

import com.sarathijh.rumia.frontend.ast.*
import com.sarathijh.rumia.frontend.lex.LexIterator
import com.sarathijh.rumia.frontend.lex.Lexer
import com.sarathijh.rumia.frontend.lex.SourceRange

class Parser(lexer: LexIterator) : RecursiveDescentParser(lexer) {

    private var savedAtomExpression: Expression? = null

    fun parse(): AstNode {
        val statementList = parseListOf(::parseStatement, expectAfter = LexIterator.EOF)
        return BlockStatement(statementList, statementList.sourceRange)
    }

    private fun parseStatement() = oneOf(
            ::parseAssignStatement,
            ::parseIfStatement,
            ::parseWhileStatement,
            ::parseForInStatement,
            ::parseReturnStatement,
            ::parseDeclaration,
            ::parseSubscriptAssignStatement,
            ::parseExpressionStatement)

    private fun parseBlockStatement(): BlockStatement? {
        val (statementList, outerSourceRange) =
                parseSurrounded("brace statement", "{", "}") {
                    parseListOf(::parseStatement)
                }
                        ?: return null

        return BlockStatement(statementList, outerSourceRange)
    }

    private fun parseExpressionStatement(): ExpressionStatement? {
        val statement = ExpressionStatement(parseExpression() ?: return null)

        expectEndOfStatement(statement.sourceRange.after)

        return statement
    }

    private fun parseAssignStatement(): AssignStatement? {
        val identifierToken = lexMatch(Lexer.TkIdentifier)
                ?: return null

        val equalsToken = oneOf("=", "+=", "-=", "*=", "%=", "&=", "|=", "^=")

        // Lookahead to check for equals; if we don't find it then put back the identifier
        return if (equalsToken != null) {
            val valueExpression = parseExpression()
                    ?: throw ParseError(
                            "Expected expression in assignment",
                            equalsToken.sourceRange.after)

            expectEndOfStatement(valueExpression.sourceRange.after)

            val identifierExpression = IdentifierExpression(identifierToken.value, identifierToken.sourceRange)

            when (equalsToken.kind) {

                "=" -> AssignStatement(identifierExpression, valueExpression)

                else -> AssignStatement(
                        identifierExpression,
                        InfixExpression(
                                equalsToken.kind.substring(0, 1),
                                identifierExpression,
                                valueExpression,
                                equalsToken.sourceRange))
            }
        } else {
            lexer.putBack(identifierToken)
            null
        }
    }

    private fun parseSubscriptAssignStatement(): Statement? {
        val subscriptExpression = parseSubscriptExpression()
                ?: return null

        val equalsToken = oneOf("=", "+=", "-=", "*=", "%=", "&=", "|=", "^=")

        // Lookahead to check for equals; if we don't find it then return the subscript expression as a statement
        return if (equalsToken != null) {
            val valueExpression = parseExpression()
                    ?: throw ParseError(
                            "Expected expression in assignment",
                            equalsToken.sourceRange.after)

            expectEndOfStatement(valueExpression.sourceRange.after)

            when (equalsToken.kind) {
                "=" -> SubscriptAssignStatement(subscriptExpression, valueExpression)
                else -> SubscriptAssignStatement(
                        subscriptExpression,
                        InfixExpression(
                                equalsToken.kind.substring(0, 1),
                                subscriptExpression,
                                valueExpression,
                                equalsToken.sourceRange))
            }
        } else {
            savedAtomExpression = subscriptExpression
            return null
        }
    }

    private fun parseIfStatement(): IfStatement? {
        val ifToken = lexMatch("if")
                ?: return null

        lexMatch(Lexer.TkNewline)

        val conditionExpression = parseExpression()
                ?: throw ParseError(
                        "Expected expression in 'if' condition",
                        ifToken.sourceRange.after)

        lexMatch(Lexer.TkNewline)

        val bodyStatement = parseBlockStatement()
                ?: throw ParseError(
                        "Expected '{' after 'if' condition",
                        conditionExpression.sourceRange.after)

        lexMatch(Lexer.TkNewline)

        val elseStatement = expectIf("else", { oneOf(
                ::parseBlockStatement,
                ::parseIfStatement)
        }, "Expected '{' after else")

        return IfStatement(
                conditionExpression,
                bodyStatement,
                elseStatement,
                SourceRange(
                        ifToken.sourceRange.start,
                        elseStatement?.sourceRange?.endInclusive
                                ?: bodyStatement.sourceRange.endInclusive))
    }

    private fun parseWhileStatement(): WhileStatement? {
        val whileToken = lexMatch("while")
                ?: return null

        lexMatch(Lexer.TkNewline)

        val conditionExpression = parseExpression()
                ?: throw ParseError(
                        "Expected expression in 'while' condition",
                        whileToken.sourceRange.after)

        lexMatch(Lexer.TkNewline)

        val bodyStatement = parseBlockStatement()
                ?: throw ParseError(
                        "Expected '{' after 'while' condition",
                        conditionExpression.sourceRange.after)

        return WhileStatement(
                conditionExpression,
                bodyStatement,
                SourceRange(whileToken.sourceRange.start, bodyStatement.sourceRange.endInclusive))
    }

    private fun parseForInStatement(): ForInStatement? {
        val forToken = lexMatch("for")
                ?: return null

        lexMatch(Lexer.TkNewline)

        val identifierExpression = parseIdentifierExpression()
                ?: throw ParseError(
                        "Expected identifier in 'for' condition",
                        forToken.sourceRange.after)

        lexMatch(Lexer.TkNewline)

        val inToken = lexMatch("in")
                ?: throw ParseError(
                        "Expected 'in' in 'for' condition",
                        identifierExpression.sourceRange.after)

        lexMatch(Lexer.TkNewline)

        val arrayExpression = parseExpression()
                ?: throw ParseError(
                        "Expected array expression in 'for' condition",
                        inToken.sourceRange.after)

        lexMatch(Lexer.TkNewline)

        val bodyStatement = parseBlockStatement()
                ?: throw ParseError(
                        "Expected '{' after 'for' condition",
                        arrayExpression.sourceRange.after)

        return ForInStatement(
                identifierExpression,
                arrayExpression,
                bodyStatement,
                SourceRange(forToken.sourceRange.start, bodyStatement.sourceRange.endInclusive))
    }

    private fun parseReturnStatement(): ReturnStatement? {
        val returnToken = lexMatch("return")
                ?: return null

        if (lexer.peek().kind == Lexer.TkNewline) {
            val newline = lexer.next()
            val expressionStatement = parseExpressionStatement()
            if (expressionStatement != null) {
                throw ParseError(
                        "Expression following 'return' must start on the same line",
                        expressionStatement.sourceRange)
            } else {
                lexer.putBack(newline)
            }
        }

        val expression = parseExpression()

        expectEndOfStatement(expression?.sourceRange?.after ?: returnToken.sourceRange.after)

        return ReturnStatement(
                expression,
                SourceRange(
                        returnToken.sourceRange.start,
                        expression?.sourceRange?.endInclusive
                                ?: returnToken.sourceRange.endInclusive))
    }

    private fun parseDeclaration() = oneOf(
            ::parseVariableDeclaration,
            ::parseFunctionDeclaration)

    private fun parseVariableDeclaration(): VariableDeclaration? {
        val varToken = lexMatch("var")
                ?: return null

        val identifierExpression = parseIdentifierExpression()
                ?: throw ParseError(
                        "Expected identifier in variable declaration",
                        varToken.sourceRange.after)

        val typeAnnotation = expectIf(":", ::parseTypeAnnotation, "Type annotation missing after ':'")
        val valueExpression = expectIf("=", ::parseExpression, "Expected initial value after '='")

        if (typeAnnotation == null && valueExpression == null) {
            throw ParseError(
                    "Expecting type annotation in variable declaration",
                    identifierExpression.sourceRange.after)
        }

        val endSourceRange = valueExpression?.sourceRange ?: typeAnnotation?.sourceRange ?: identifierExpression.sourceRange

        expectEndOfStatement(endSourceRange.after)

        return VariableDeclaration(
                identifierExpression,
                typeAnnotation,
                valueExpression,
                SourceRange(varToken.sourceRange.start, endSourceRange.endInclusive))
    }

    private fun parseFunctionDeclaration(): FunctionDeclaration? {
        val funToken = lexMatch("fun")
                ?: return null

        lexMatch(Lexer.TkNewline)

        val identifierExpression = parseIdentifierExpression()
                ?: throw ParseError(
                        "Expected identifier in function declaration",
                        funToken.sourceRange.after)

        lexMatch(Lexer.TkNewline)

        val parametersSurrounded = parseSurrounded("parameter list", "(", ")") {
            parseDelimitedListOf(::parseFunctionParameter, delimiter = ",", itemPrefix = Lexer.TkIdentifier)
        }
                ?: throw ParseError(
                        "Expected '(' before parameter list",
                        identifierExpression.sourceRange.after)

        val (parameterList, parametersOuterSourceRange) = parametersSurrounded

        val returnTypeAnnotation = expectIf("->", ::parseTypeAnnotation, "Expected type for function result")

        lexMatch(Lexer.TkNewline)

        val bodyStatement = parseBlockStatement()
                ?: throw ParseError(
                        "Expected '{' in body of function declaration",
                        returnTypeAnnotation?.sourceRange?.after
                                ?: parametersOuterSourceRange.after)

        return FunctionDeclaration(
                identifierExpression,
                returnTypeAnnotation,
                parameterList,
                bodyStatement,
                false,
                SourceRange(funToken.sourceRange.start, bodyStatement.sourceRange.endInclusive))
    }

    private fun parseFunctionParameter(): FunctionParameter? {
        val identifierExpression = parseIdentifierExpression()
                ?: return null

        lexMatch(Lexer.TkNewline)

        val typeAnnotation = expectIf(":", ::parseTypeAnnotation, "Type annotation missing after ':'")
                ?: throw ParseError(
                        "Parameter requires an explicit type",
                        identifierExpression.sourceRange.after)

        return FunctionParameter(identifierExpression, typeAnnotation)
    }

    private fun expectEndOfStatement(sourceRange: SourceRange) {
        val separator = lexMatch(Lexer.TkNewline) ?: lexMatch(LexIterator.EOF)

        if (separator == null && lexer.peek().kind != "}") {
            throw ParseError(
                    "Consecutive statements are not allowed on the same line",
                    sourceRange)
        }
    }

    private fun parseExpression() = parseInfixExpression(0)

    private fun parseInfixExpression(precedence: Int): Expression? {
        // Uses the "precedence climbing" algorithm described here:
        // http://www.engr.mun.ca/~theo/Misc/exp_parsing.htm#climbing

        var term = parseAtomExpression()

        while (term != null) {
            val operator = Operator.fromSymbol(lexer.peek().value)
                    ?: break

            if (operator.isPrefix || operator.precedence < precedence) {
                break
            }

            val operatorToken = lexer.next()

            lexMatch(Lexer.TkNewline)

            term = if (operatorToken.value == ".") {
                val nextTerm = parseIdentifierExpression()
                        ?: throw ParseError(
                                "Expected identifier after member access operator",
                                operatorToken.sourceRange.after)

                MemberAccessExpression(term, nextTerm)
            } else {
                val q = operator.precedence + (if (operator.isRightAssociative) 0 else 1)
                val nextTerm = parseInfixExpression(q)
                        ?: throw ParseError(
                                "Expected expression after infix operator",
                                operatorToken.sourceRange.after)

                InfixExpression(operator.symbol, term, nextTerm, operatorToken.sourceRange)
            }
        }

        return term
    }

    private fun parseAtomExpression() = if (savedAtomExpression != null) {
        val atom = savedAtomExpression
        savedAtomExpression = null
        atom
    } else {
        oneOf(
                ::parsePrefixExpression,
                ::parseSubscriptExpression,
                ::parseFunctionCallExpression,
                ::parseIdentifierExpression,
                ::parseTypeNameExpression,
                ::parseIntLiteralExpression,
                ::parseBoolLiteralExpression,
                ::parseStringLiteralExpression,
                ::parseArrayLiteralExpression,
                ::parseParenthesizedExpression)
    }

    private fun parsePrefixExpression(): Expression? {
        val operator = Operator.fromSymbol("prefix${lexer.peek().value}")
                ?: return null

        val operatorToken = lexer.next()

        val term = parseInfixExpression(operator.precedence)
                ?: throw ParseError(
                        "Expected expression after prefix operator",
                        operatorToken.sourceRange.after)

        return PrefixExpression(operatorToken.value, term, operatorToken.sourceRange)
    }

    private fun parseSubscriptExpression(): SubscriptExpression? {
        val identifierToken = lexMatch(Lexer.TkIdentifier)
                ?: return null

        // TODO: If parseExpression returns null then parse error should be thrown. This avoids 'a[]'.
        val subscriptSurrounded = parseSurrounded("subscript index", "[", "]", ::parseExpression)

        return if (subscriptSurrounded != null) {

            val (subscriptIndex, subscriptOuterSourceRange) = subscriptSurrounded

            SubscriptExpression(
                    IdentifierExpression(identifierToken.value, identifierToken.sourceRange),
                    subscriptIndex,
                    SourceRange(identifierToken.sourceRange.start, subscriptOuterSourceRange.endInclusive))
        } else {
            lexer.putBack(identifierToken)
            null
        }
    }

    private fun parseFunctionCallExpression(): FunctionCallExpression? {
        val identifierToken = lexMatch(Lexer.TkIdentifier)
                ?: return null

        val argumentsSurrounded = parseSurrounded("argument list", "(", ")") {
            parseDelimitedListOf(::parseExpression, delimiter = ",")
        }

        return if (argumentsSurrounded != null) {

            val (argumentList, argumentsOuterSourceRange) = argumentsSurrounded

            return FunctionCallExpression(
                    IdentifierExpression(identifierToken.value, identifierToken.sourceRange),
                    argumentList,
                    SourceRange(identifierToken.sourceRange.start, argumentsOuterSourceRange.endInclusive))
        } else {
            lexer.putBack(identifierToken)
            null
        }
    }

    private fun parseIdentifierExpression(): IdentifierExpression? {
        val identifierToken = lexMatch(Lexer.TkIdentifier)
                ?: return null

        return IdentifierExpression(identifierToken.value, identifierToken.sourceRange)
    }

    private fun parseTypeNameExpression(): TypeNameExpression? {
        val typeAnnotation = parseTypeAnnotation()
                ?: return null

        return TypeNameExpression(typeAnnotation)
    }

    private fun parseIntLiteralExpression(): IntLiteralExpression? {
        val intLiteralToken = lexMatch(Lexer.TkIntLiteral)
                ?: return null

        val intString = intLiteralToken.value

        try {
            val intValue = when {
                intString.startsWith("0b") -> intString.substring(2).toInt(2)
                intString.startsWith("0x") -> intString.substring(2).toInt(16)
                else -> intString.toInt()
            }

            if (intValue > 0xFFFF) {
                throw ParseError(
                        "No type large enough to store value '$intValue'",
                        intLiteralToken.sourceRange)
            }

            return IntLiteralExpression(intValue, intLiteralToken.sourceRange)

        } catch (ex: NumberFormatException) {
            throw ParseError(
                    "No type large enough to store value '$intString'",
                    intLiteralToken.sourceRange)
        }
    }

    private fun parseBoolLiteralExpression(): BoolLiteralExpression? {
        val boolLiteralToken = lexMatch(Lexer.TkBoolLiteral)
                ?: return null

        return BoolLiteralExpression(boolLiteralToken.value == "true", boolLiteralToken.sourceRange)
    }

    private fun parseStringLiteralExpression(): StringLiteralExpression? {
        val stringLiteralToken = lexMatch(Lexer.TkStringLiteral)
                ?: return null

        val stringValue = stringLiteralToken.value
        return StringLiteralExpression(stringValue, stringLiteralToken.sourceRange)
    }

    private fun parseArrayLiteralExpression(): ArrayLiteralExpression? {
        val surrounded = parseSurrounded("array literal expression", "[", "]") {
            parseDelimitedListOf(::parseExpression, ",")
        }
                ?: return null

        val (expressionList, outerSourceRange) = surrounded

        return ArrayLiteralExpression(expressionList, outerSourceRange)
    }

    private fun parseParenthesizedExpression(): Expression? {
        // TODO: If a '(' is found, but no expression, then an error should be thrown. This avoids '()'.
        val (expression, _) = parseSurrounded("parenthesized expression", "(", ")", ::parseExpression)
                ?: return null

        return expression
    }

    private fun parseTypeAnnotation(): TypeAnnotation? {
        val typeToken = lexMatch(Lexer.TkTypeName)
                ?: lexMatch(Lexer.TkIdentifier)
                ?: return null

        val typeAnnotation = when (typeToken.value) {
            "Int" -> IntTypeAnnotation(typeToken.sourceRange)
            "Int16" -> Int16TypeAnnotation(typeToken.sourceRange)
            "UInt" -> UIntTypeAnnotation(typeToken.sourceRange)
            "UInt16" -> UInt16TypeAnnotation(typeToken.sourceRange)
            "Bool" -> BoolTypeAnnotation(typeToken.sourceRange)
            "String" -> StringTypeAnnotation(typeToken.sourceRange)
            else -> ClassTypeAnnotation(typeToken.value, typeToken.sourceRange)
        }

        val sizeExpression = expectIf("[", ::parseIntLiteralExpression, "Expected size in array type")
                ?: return typeAnnotation

        lexMatch("]")
                ?: throw ParseError(
                        "Expected ']' in array type",
                        sizeExpression.sourceRange.after)

        return ArrayTypeAnnotation(
                typeAnnotation,
                sizeExpression,
                SourceRange(typeToken.sourceRange.start, sizeExpression.sourceRange.endInclusive))
    }
}
