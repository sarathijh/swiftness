package com.sarathijh.rumia.frontend.lex

import java.util.*
import java.util.regex.Pattern

data class Token(val kind: String, val value: String, val sourceRange: SourceRange)

class LexError(override val message: String, val sourceRange: SourceRange) : Throwable()

open class LexIterator(private val scanner: Scanner) : Iterator<Token> {

    companion object {
        const val EOF = "EOF"
    }

    private var previousLine = 0
    private var previousColumn = 0
    private var currentLine = 0
    private var currentColumn = 0

    private var currentToken: Token? = null
    private var nextToken: Token? = null

    private val tokenPatterns = mutableMapOf<Pattern, String?>()
    private val tokenTransforms = mutableMapOf<Pattern, (String) -> String>()
    private val ignorePatterns = mutableListOf<Pattern>()
    private val ignoreRanges = mutableMapOf<Pattern, Pattern>()

    fun addTokenPattern(regex: String, tag: String? = null, transform: ((String) -> String)? = null) {
        val pattern = Pattern.compile("\\G($regex)")
        tokenPatterns[pattern] = tag
        if (transform != null) {
            tokenTransforms[pattern] = transform
        }
    }

    fun addIgnorePattern(regex: String) {
        ignorePatterns.add(Pattern.compile("\\G($regex)"))
    }

    fun addIgnoreRange(beforeRegex: String, afterRegex: String) {
        ignoreRanges[Pattern.compile("\\G($beforeRegex)")] = Pattern.compile("\\G(.*?$afterRegex)", Pattern.DOTALL)
    }

    override fun hasNext() = (nextToken().kind != EOF)

    override fun next(): Token {
        val token = nextToken()
        currentToken = nextToken
        nextToken = null
        return token
    }

    fun peek() = nextToken()

    fun putBack(token: Token) {
        nextToken = currentToken
        currentToken = token
    }

    private fun nextToken(): Token {
        if (currentToken == null) {
            while (matchIgnorePattern() || matchIgnoreRange()) {}
            currentToken = matchTokenPattern()
        }

        if (currentToken == null && scanner.hasNext()) {
            throw LexError(
                    "Unexpected '${scanner.next()[0]}'",
                    SourceRange(
                            SourceLocation(currentLine, currentColumn),
                            SourceLocation(currentLine, currentColumn)))
        }

        return currentToken
                ?: Token(EOF, EOF,
                        SourceRange(
                                SourceLocation(currentLine, currentColumn),
                                SourceLocation(currentLine, currentColumn)))
    }

    private fun matchIgnorePattern(): Boolean {
        for (pattern in ignorePatterns) {
            val value = consumePattern(pattern)
            if (value != null) {
                return true
            }
        }
        return false
    }

    private fun matchIgnoreRange(): Boolean {
        for ((beforePattern, afterPattern) in ignoreRanges) {
            if (consumePattern(beforePattern) != null) {
                consumePattern(afterPattern)
                return true
            }
        }
        return false
    }

    private fun matchTokenPattern(): Token? {
        for ((pattern, kind) in tokenPatterns) {
            val value = consumePattern(pattern)
            if (value != null) {
                val transformedValue = tokenTransforms[pattern]?.invoke(value) ?: value
                val columnEndInclusive = (if (transformedValue.isNotEmpty() && transformedValue.last() == '\n') previousColumn + value.length else currentColumn) - 1
                return Token(
                        kind ?: transformedValue,
                        transformedValue,
                        SourceRange(
                                SourceLocation(previousLine, previousColumn),
                                SourceLocation(previousLine, columnEndInclusive)))
            }
        }
        return null
    }

    private fun consumePattern(pattern: Pattern): String? {
        if (scanner.findWithinHorizon(pattern, 0) != null) {
            val result = scanner.match().group(0)
            updateLineAndColumnForMatch(result)
            return result
        }
        return null
    }

    private fun updateLineAndColumnForMatch(match: String) {
        previousColumn = currentColumn
        previousLine = currentLine

        val lastNewLine = match.lastIndexOf('\n')
        if (lastNewLine >= 0) {
            val numberOfNewLines = match.numberOf('\n')
            currentLine += numberOfNewLines
            currentColumn = match.length - lastNewLine - 1
        } else {
            currentColumn += match.length
        }
    }

    private fun String.numberOf(char: Char) = this.fold(0) { acc, c -> acc + (if (c == char) 1 else 0) }
}
