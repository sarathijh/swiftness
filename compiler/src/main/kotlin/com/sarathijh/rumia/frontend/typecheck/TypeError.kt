package com.sarathijh.rumia.frontend.typecheck

import com.sarathijh.rumia.frontend.lex.Lexer
import com.sarathijh.rumia.frontend.lex.SourceRange
import com.sarathijh.rumia.util.bigramComparison

open class TypeError(override val message: String, val sourceRange: SourceRange) : Throwable()

class TypeMismatchError(val expectedType: Type, val givenType: Type, sourceRange: SourceRange)
    : TypeError("'$givenType' is not convertible to '$expectedType'", sourceRange)

// TODO: Keep track of where names were declared to give more error information
class InvalidRedeclarationError(val name: String, sourceRange: SourceRange)
    : TypeError("Invalid redeclaration of '$name'", sourceRange)

class ArgumentListMismatchError(val functionName: String, val argumentTypes: List<Type>, val functionSetType: FunctionSetType, sourceRange: SourceRange)
    : TypeError("", sourceRange) {

    override val message by lazy {
        val argumentList = argumentTypes.joinToString(", ")
        val candidateList = functionSetType.functionTypes.joinToString("\n") {
            "    $functionName(${it.parameterTypes.joinToString(", ")})"
        }

        """Cannot invoke '$functionName' with an argument list of type '($argumentList)'

            Candidates are:
            $candidateList"""
    }
}

class UnresolvedIdentifierError(val identifier: String, val typeEnvironment: TypeEnvironment, sourceRange: SourceRange)
    : TypeError("", sourceRange) {

    override val message by lazy {
        val mostSimilarName = getMostSimilarName(0.5f)
        val nameSuggestion = if (mostSimilarName != null) {
            "\n\n    Did you mean '$mostSimilarName'?"
        } else {
            ""
        }

        "Use of unresolved identifier '$identifier'$nameSuggestion"
    }

    private fun getMostSimilarName(similarityPercentageThreshold: Float): String? {
        val mostSimilarDeclaredName = typeEnvironment.allDeclaredNames
                .filter { it.matches(Regex(Lexer.identifierPattern)) }
                .map { bigramComparison(identifier, it) to it }
                .maxBy { it.first }
                ?: return null

        return if (mostSimilarDeclaredName.first >= similarityPercentageThreshold) {
            mostSimilarDeclaredName.second
        } else {
            null
        }
    }
}
