package com.sarathijh.rumia.frontend.lex

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.util.*

internal class LexIteratorTest {

    private fun createLexIterator(scanner: Scanner) = LexIterator(scanner).apply {
        addTokenPattern("\\b[a-zA-Z]+\\b")
        addTokenPattern("\\d+")
        addTokenPattern("\n+", "Newline")
        addTokenPattern("\"((?<=\\\\)\"|[^\"])*\"", "String") {
            var content = it.substring(1, it.length - 1)
            content = content.replace("\\n", "\n")
            content = content.replace("\\n", "\n")
            content = content.replace("\\\"", "\"")
            content
        }

        addIgnorePattern("[ \t]+")
        addIgnorePattern("//.*?(?=\n)")
        addIgnoreRange("/\\*", "\\*/")
    }

    @Test fun `has next`() {
        val lexIterator = createLexIterator(Scanner("hello hello hello"))

        for (i in 0 until 3) {
            assertTrue(lexIterator.hasNext())
            lexIterator.next()
        }
        assertFalse(lexIterator.hasNext())
    }

    @Test fun next() {
        val lexIterator = createLexIterator(Scanner("hello world 12345"))

        val expectedTokens = listOf("hello", "world", "12345")
        for ((expected, found) in expectedTokens.zip(lexIterator.asSequence().toList())) {
            assertEquals(expected, found.value)
        }
    }

    @Test fun peek() {
        val lexIterator = createLexIterator(Scanner("hello world 12345"))

        for (i in 0 until 10) {
            assertEquals("hello", lexIterator.peek().value)
        }
    }

    @Test fun `put back`() {
        val lexIterator = createLexIterator(Scanner("hello world 12345"))

        assertEquals("hello", lexIterator.next().value)

        val worldToken = lexIterator.next()
        assertEquals("world", worldToken.value)
        lexIterator.putBack(worldToken)

        assertEquals("world", lexIterator.next().value)
        assertEquals("12345", lexIterator.next().value)
    }

    @Test fun ignore() {
        val lexIterator = createLexIterator(Scanner("""
            hello /* block comment */ world
            foobar // line comment
            another
        """.trimIndent()))

        assertEquals("hello", lexIterator.next().value)
        assertEquals("world", lexIterator.next().value)
        assertEquals("Newline", lexIterator.next().kind)
        assertEquals("foobar", lexIterator.next().value)
        assertEquals("Newline", lexIterator.next().kind)
        assertEquals("another", lexIterator.next().value)
        assertEquals(LexIterator.EOF, lexIterator.next().value)
    }

    @Test fun newlines() {
        val lexIterator = createLexIterator(Scanner("""
            hello // comment

            // comment 2

            what
        """.trimIndent()))

        assertEquals("hello", lexIterator.next().value)
        assertEquals("Newline", lexIterator.next().kind)
        assertEquals("Newline", lexIterator.next().kind)
        assertEquals("what", lexIterator.next().value)
        assertEquals(LexIterator.EOF, lexIterator.next().value)
    }

    @Test fun transform() {
        val lexIterator = createLexIterator(Scanner("123 \"This is a \\n string\""))

        assertEquals("123", lexIterator.next().value)
        assertEquals("This is a \n string", lexIterator.next().value)
    }

    @Test fun `matches from current location`() {
        val lexIterator = createLexIterator(Scanner("12345 hello"))

        // Because the pattern for words is checked first,
        // this tests that 'hello' isn't matched by skipping past '12345'
        assertEquals("12345", lexIterator.next().value)
    }

    @Test fun error() {
        val lexIterator = createLexIterator(Scanner("valid _invalid"))

        lexIterator.next()
        assertThrows(LexError::class.java) {
            lexIterator.next()
        }
    }
}
