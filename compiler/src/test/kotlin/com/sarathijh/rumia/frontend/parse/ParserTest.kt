package com.sarathijh.rumia.frontend.parse

import com.sarathijh.rumia.frontend.ast.*
import com.sarathijh.rumia.frontend.lex.Lexer
import com.sarathijh.rumia.frontend.lex.SourceLocation
import com.sarathijh.rumia.frontend.lex.SourceRange
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

internal class ParserTest {
    @Test fun identifier() {
        val source = "myVariable"
        assertParse(source, IdentifierExpression(source, sourceRange(0, 0..(source.length-1))))
    }

    @Test fun `decimal integer literal`() = assertIntLiteral("0", 0)
    @Test fun `hex integer literal`() = assertIntLiteral("0x5D", 0x5D)
    @Test fun `binary integer literal`() = assertIntLiteral("0b101101", 0b101101)

    private fun assertIntLiteral(source: String, value: Int) {
        assertParse(source, IntLiteralExpression(value, sourceRange(0, 0..(source.length-1))))
    }

    @Test fun `true bool literal`() = assertBoolLiteral("true", true)
    @Test fun `false bool literal`() = assertBoolLiteral("false", false)

    private fun assertBoolLiteral(source: String, value: Boolean) {
        assertParse(source, BoolLiteralExpression(value, sourceRange(0, 0..(source.length-1))))
    }

    @Test fun `string literal`() {
        val stringValue = "Hello World!"
        val source = "\"$stringValue\""

        assertParse(source, StringLiteralExpression(stringValue, sourceRange(0, 0..(source.length-1))))
    }

    @Test fun `empty array literal`() {
        val source = "[]"

        val expected = ArrayLiteralExpression(
                AstNodeList(listOf()),
                sourceRange(0, 0..(source.length-1)))

        assertParse(source, expected)
    }

    @Test fun `single element array literal`() {
        val source = "[1]"

        val expected = ArrayLiteralExpression(
                AstNodeList(listOf(
                        IntLiteralExpression(1, sourceRange(0, 1))
                )),
                sourceRange(0, 0..(source.length-1)))

        assertParse(source, expected)
    }

    @Test fun `multi-element array literal`() {
        val source = "[1, 2, 3, 4, 5]"

        val expected = ArrayLiteralExpression(
                AstNodeList(
                        listOf(
                                IntLiteralExpression(1, sourceRange(0, 1)),
                                IntLiteralExpression(2, sourceRange(0, 4)),
                                IntLiteralExpression(3, sourceRange(0, 7)),
                                IntLiteralExpression(4, sourceRange(0, 10)),
                                IntLiteralExpression(5, sourceRange(0, 13))
                        )),
                sourceRange(0, 0..(source.length-1)))

        assertParse(source, expected)
    }

    @Test fun `variable declaration with type annotation and initial value`() {
        val source = "var myVariable: Int = 1"

        val expected = VariableDeclaration(
                IdentifierExpression("myVariable", sourceRange(0, 4..13)),
                IntTypeAnnotation(sourceRange(0, 16..18)),
                IntLiteralExpression(1, sourceRange(0, 22)),
                sourceRange(0, 0..(source.length-1)))

        assertParse(source, expected)
    }

    @Test fun `variable declaration with initial value`() {
        val source = "var myVariable = 1"

        val expected = VariableDeclaration(
                IdentifierExpression("myVariable", sourceRange(0, 4..13)),
                null,
                IntLiteralExpression(1, sourceRange(0, 17)),
                sourceRange(0, 0..(source.length-1)))

        assertParse(source, expected)
    }

    @Test fun `variable declaration with type annotation`() {
        val source = "var myVariable: Int"

        val expected = VariableDeclaration(
                IdentifierExpression("myVariable", sourceRange(0, 4..13)),
                IntTypeAnnotation(sourceRange(0, 16..18)),
                null,
                sourceRange(0, 0..(source.length-1)))

        assertParse(source, expected)
    }

    @Test fun `variable assignment`() {
        val source = "myVariable = 1"

        val expected = AssignStatement(
                IdentifierExpression("myVariable", sourceRange(0, 0..9)),
                IntLiteralExpression(1, sourceRange(0, 13)))

        assertParse(source, expected)
    }

    @Test fun `subscript assignment`() {
        val source = "myVariable[0] = 1"

        val expected = SubscriptAssignStatement(
                SubscriptExpression(
                        IdentifierExpression("myVariable", sourceRange(0, 0..9)),
                        IntLiteralExpression(0, sourceRange(0, 11)),
                        sourceRange(0, 0..12)),
                IntLiteralExpression(1, sourceRange(0, 16)))

        assertParse(source, expected)
    }

    @Test fun `function call with no arguments`() {
        val source = "myFunction()"

        val expected = FunctionCallExpression(
                IdentifierExpression("myFunction", sourceRange(0, 0..9)),
                AstNodeList(listOf()),
                sourceRange(0, 0..11))

        assertParse(source, expected)
    }

    @Test fun `function call with 1 argument`() {
        val source = "myFunction(20)"

        val expected = FunctionCallExpression(
                IdentifierExpression("myFunction", sourceRange(0, 0..9)),
                AstNodeList(listOf(IntLiteralExpression(20, sourceRange(0, 11..12)))),
                sourceRange(0, 0..13))

        assertParse(source, expected)
    }

    private fun assertParse(source: String, expected: Statement) {
        assertEquals(
                BlockStatement(
                        AstNodeList(listOf(expected)),
                        expected.sourceRange),
                Parser(Lexer(Scanner(source))).parse())
    }

    private fun assertParse(source: String, expected: Expression) = assertParse(source, ExpressionStatement(expected))

    private fun sourceRange(line: IntRange, column: IntRange) = SourceRange(SourceLocation(line.start, column.start), SourceLocation(line.endInclusive, column.endInclusive))
    private fun sourceRange(line: Int, column: Int) = sourceRange(line..line, column..column)
    private fun sourceRange(line: Int, column: IntRange) = sourceRange(line..line, column)
}
