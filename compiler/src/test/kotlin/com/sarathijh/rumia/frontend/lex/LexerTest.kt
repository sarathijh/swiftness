package com.sarathijh.rumia.frontend.lex

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

internal class LexerTest {
    @Test fun identifier() {
        Lexer(Scanner("a b hello_world")).apply {
            assertNextIs(Lexer.TkIdentifier, "a")
            assertNextIs(Lexer.TkIdentifier, "b")
            assertNextIs(Lexer.TkIdentifier, "hello_world")
        }
    }

    private fun Lexer.assertNextIs(expectedKind: String, expectedValue: String) = next().assertIs(expectedKind, expectedValue)

    private fun Token.assertIs(expectedKind: String, expectedValue: String) {
        assertEquals(expectedKind, kind)
        assertEquals(expectedValue, value)
    }
}
